﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Web.Models;

namespace SecondHand.Web.Controllers
{
    public class HomeController : Controller
    {
        ICategoryQueries _categoryQueries;

        public HomeController(ICategoryQueries categoryQueries)
        {
            _categoryQueries = categoryQueries;
        }

        [HttpGet("", Name = "home")]
        public async Task<IActionResult> Index()
        {
            var categoryTree = await _categoryQueries.GetCategoryTree();
            var sfdf = categoryTree.GetLevel(0);
            var model = new HomeViewModel();
            model.CategorySections.AddRange(sfdf.Select(x => new SectionCategories {
                Name = x.Name,
                Categories = x.Child.Select(z => new Category {
                    Id = z.Id,
                    Name = z.Name,
                    ImagePath = z.ImagePath
                })
            }));

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
