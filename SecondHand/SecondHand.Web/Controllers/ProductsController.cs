﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Web.Models;

namespace SecondHand.Web.Controllers
{
    public class ProductsController : Controller
    {
        [HttpGet("[action]/{categoryId}/[controller]", Name = "getProductsByCategory")]
        public async Task<IActionResult> Category(string categoryId, [FromServices]ICategoryQueries categoryQueries)
        {
            var categoryTree = await categoryQueries.GetCategoryTreeWithProducts(categoryId);
            var node = categoryTree.FindCategoryById(categoryId);
            var model = new SectionProducts {
                Name = node.Name,
                ProductPreviews = node.GetAllProducts().Select(x => new ProductPreview {
                    Id = x.Id,
                    Label = x.Label,
                    Images = x.Images,
                    IsHidden = x.IsHidden,
                    Price = x.Price
                })
            };

            return View("Products", model);
        }

        [HttpGet("[controller]/{productId}/[action]", Name = "getProductDetails")]
        public async Task<IActionResult> Details(string productId, [FromServices]IProductQueries productQueries)
        {
            return View(await productQueries.GetProductDescription(productId));
        }
    }
}