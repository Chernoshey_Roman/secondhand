﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Models
{
    public class SectionProducts
    {
        public SectionProducts()
        {
            ProductPreviews = new List<ProductPreview>();
        }

        public string Name { get; set; }
        public IEnumerable<ProductPreview> ProductPreviews { get; set; }
    }
}
