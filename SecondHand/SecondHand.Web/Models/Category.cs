﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Models
{
    public class Category
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
    }
}
