﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Models
{
    public class SectionCategories
    {
        public SectionCategories()
        {
            Categories = new List<Category>();
        }

        public string Name { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
