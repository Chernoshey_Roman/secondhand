﻿using System;
using System.Collections.Generic;
using SecondHand.Application.Catalog.ProductAggregate;

namespace SecondHand.Web.Models
{
    public class ProductPreview
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public List<ProductImageSnapshot> Images { get; set; }
        public bool IsHidden { get; set; }
        public decimal Price { get; set; }
    }
}