﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;

namespace SecondHand.Web.Models.NavigationBuilder
{
    public class CategoriesMainMenuBuilder : AbstractVisitor
    {
        IUrlHelper _urlHelper;
        public HtmlContentBuilder Html { get; private set; }

        public CategoriesMainMenuBuilder(IUrlHelper urlHelper)
        {
            Html = new HtmlContentBuilder();
            this._urlHelper = urlHelper;
        }

        public override void Visit(CategoryQuery category, uint level)
        {
            if (category.IsNotDisplayed) return;

            var url = _urlHelper.RouteUrl("getProductsByCategory", new { categoryId = category.Id });
            var itemClasses = "main-menu__item";
            string icon = string.Empty;
                
            if (category.Child.Where(x => x.IsNotDisplayed != true).Any())
            {
                icon += "<i class=\"fas fa-angle-right main-menu__arrow\"></i>";
            }

            Html.AppendHtml($"<li class=\"{itemClasses}\"><a href=\"{url}\" class=\"main-menu__link\">{category.Name}{icon}</a>");

            if (category.Child != null && category.Child.Any())
            {
                Html.AppendHtml("<ul class=\"main-menu__sub-list\">");

                foreach (var item in category.Child)
                {
                    item.Accept(this, level);
                }

                Html.AppendHtml("</ul>");
            }

            Html.AppendHtml("</li>");
        }

        public override void Visit(CategoryTree categoryTree)
        {
            foreach (var item in categoryTree.GetLevel(0))
            {
                item.Accept(this, 0);
            }
        }
    }
}
