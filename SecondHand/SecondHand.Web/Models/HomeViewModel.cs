﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Models
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {
            CategorySections = new List<SectionCategories>();
            ProductSections = new List<SectionProducts>();
        }
        public List<SectionCategories> CategorySections { get; set; }
        public List<SectionProducts> ProductSections { get; set; }
    }
}
