﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.ProductAggregate.Add;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Remove;
using SecondHand.Application.Catalog.ProductAggregate.Update;
using SecondHand.Web.Areas.BackOffice.Extensions;
using SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder;
using SecondHand.Web.Areas.BackOffice.Models.Products;

namespace SecondHand.Web.Areas.BackOffice.Controllers
{
    [Area("BackOffice")]
    [Route("backoffice/[controller]")]
    //[Authorize]
    public class ProductsController : Controller
    {
        private NavigationBuilder _navigationBuilder;
        private IMapper _mapper;

        public ProductsController(NavigationBuilder navigationBuilder, IMapper mapper)
        {
            _navigationBuilder = navigationBuilder;
            _mapper = mapper;
        }


        [HttpGet("", Name = "getToEditProductsIndex")]
        public async Task<IActionResult> Index()
        {
            var model = new Models.Products.Index();
            model.CategoryOptions = await _navigationBuilder.GetCategoryFlatReport();
            return View(model);
        }

        [HttpGet("{id}/[action]", Name = "getToEditProduct")]
        public async Task<IActionResult> Edit(string id, [FromServices]IProductQueries productQueries)
        {
            var product = await productQueries.GetProductDescription(id);
            var vm = _mapper.Map<Edit>(product);

            vm.CategoryOptions = await _navigationBuilder.GetCategoryFlatReport();
            vm.PersistedImages = product.Images.Select(x => new PersistedImage { Path = x.Path, IsPreview = x.IsPreview }).ToList();

            return View(vm);
        }

        [HttpPost("{id}/[action]", Name = "editProduct")]
        public async Task<IActionResult> Edit(string id, Edit data, [FromServices] ProductUpdateHandler handler)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<ProductUpdateRequest>(data);

                request.Id = id;
                request.NewImageFiles = await data.NewImages.SaveFilesTemporarilyAsync();

                await handler.Handle(request);

                if (Request.IsAjaxRequest())
                {
                    return Json(new { redirectUrl = this.Url.RouteUrl("getToEditProductsIndex") });
                }

                return RedirectToRoute("getToEditProductsIndex");
            }

            return BadRequest(ModelState);
        }

        [HttpGet("[action]", Name = "getToAddProduct")]
        public async Task<IActionResult> Add()
        {
            var model = new Add();

            model.CategoryOptions = await this._navigationBuilder.GetCategoryFlatReport();

            return View(model);
        }

        [HttpPost("[action]", Name = "addProduct")]
        public async Task<IActionResult> Add(Add data, [FromServices] ProductAddHandler handler)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<ProductAddRequest>(data);

                request.NewImageFiles = await data.NewImages.SaveFilesTemporarilyAsync();
                await handler.Handle(request);

                if (Request.IsAjaxRequest())
                {
                    return Json(new { redirectUrl = this.Url.RouteUrl("getToEditProductsIndex") });
                }

                return RedirectToRoute("getToEditProductsIndex");
            }

            return BadRequest(ModelState);
        }

        [HttpPost("{id}/[action]", Name = "removeProduct")]
        public async Task<IActionResult> Remove(string id, [FromServices]ProductRemoveHandler handler)
        {
            await handler.Handle(new ProductRemoveRequest(id));

            if (Request.IsAjaxRequest())
            {
                return Json(new { redirectUrl = this.Url.RouteUrl("getToEditProductsIndex") });
            }

            return RedirectToRoute("getToEditProductsIndex");
        }
    }
}