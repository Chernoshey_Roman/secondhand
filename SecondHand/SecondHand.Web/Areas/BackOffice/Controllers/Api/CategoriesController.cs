﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application;
using SecondHand.Application.Catalog.CategoryAggregate.Add;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Application.Catalog.CategoryAggregate.Remove;
using SecondHand.Application.Catalog.CategoryAggregate.Update;
using SecondHand.Web.Areas.BackOffice.Extensions;
using SecondHand.Web.Areas.BackOffice.Models.Categories;

namespace SecondHand.Web.Areas.BackOffice.Controllers.Api
{
    [Area("BackOffice")]
    [Route("backoffice/api/[controller]")]
    [ApiController]
    //[Authorize]
    public class CategoriesController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<IList<CategoryQuery>>> Get([FromServices] ICategoryQueries categoryQueries)
        {
            var tree = await categoryQueries.GetCategoryTree();

            return tree.GetLevel(0).ToList();
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryQuery>> Get(string id, [FromServices]ICategoryQueries categoryQueries)
        {
            var category = await categoryQueries.GetCategoryDescriptionAsync(id);

            CorsTemporarySolution.PerformTemporarily(category, Request);

            return category;
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(string id, [FromForm]Edit data, [FromServices]CategoryUpdateHandler handler, [FromServices]IMapper mapper)
        {
            if (ModelState.IsValid)
            {
                var request = mapper.Map<CategoryUpdateRequest>(data);

                request.CategoryId = id;

                request.ImageFile = await data.Image.SaveFileTemporarilyAsync();                 

                await handler.Handle(request);

                return Ok();
            }

            return BadRequest(ModelState);
        }        


        [HttpPost("{id}")]
        public async Task<IActionResult> CreateAsChild(string id, [FromForm] CreateAsChild data, [FromServices]CategoryAddHandler handler, [FromServices]IMapper mapper)
        {
            if (ModelState.IsValid)
            {

                var request = mapper.Map<CategoryAddRequest>(data);
                request.SelectedCategoryId = id;
                request.ImageFile = await data.Image.SaveFileTemporarilyAsync();

                await handler.Handle(request);

                return Ok();
            }

            return BadRequest(ModelState);
        }


        [HttpPost]
        public async Task<IActionResult> CreateRoot([FromForm] CreateRoot data, [FromServices]CategoryAddHandler handler, [FromServices]IMapper mapper)
        {
            if (ModelState.IsValid)
            {
                var request = mapper.Map<CategoryAddRequest>(data);
                request.ImageFile = await data.Image.SaveFileTemporarilyAsync();

                await handler.Handle(request);

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(string id, [FromServices]CategoryRemoveHandler handler)
        {
            try
            {
                await handler.Handle(new CategoryRemoveRequest(id));
            }
            catch (BusinessException e)
            {
                return StatusCode(409, new { e.Message });
            }

            return Ok();
        }
    }
}