﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.ProductAggregate.Add;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Remove;
using SecondHand.Application.Catalog.ProductAggregate.Update;
//using SecondHand.Application.Catalog.Product.Add;
//using SecondHand.Application.Catalog.Product.Queries;
//using SecondHand.Application.Catalog.Product.Remove;
//using SecondHand.Application.Catalog.Product.Update;
using SecondHand.Web.Areas.BackOffice.Extensions;
using SecondHand.Web.Areas.BackOffice.Models.Products;
using SecondHand.Web.Areas.BackOffice.Models.Products.Api;

namespace SecondHand.Web.Areas.BackOffice.Controllers.api
{
    [Area("BackOffice")]
    [Route("backoffice/api/[controller]")]
    [ApiController]
    //[Authorize]
    public class ProductsController : ControllerBase
    {
        IMapper _mapper;

        public ProductsController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductHAL>>> Get(string categoryId, [FromServices] IProductQueries productQueries)
        {
            List<ProductDataView> result;

            if (!string.IsNullOrEmpty(categoryId) && categoryId.Equals("_without"))
            {
                result = await productQueries.GetUncategorizedProducts();
            }
            else if (!string.IsNullOrEmpty(categoryId))
            {
                result = await productQueries.GetAllProductsBy(categoryId);
            }
            else
            {
                result = await productQueries.GetAllProductsBy();
            }

            CorsTemporarySolution.PerformTemporarily(result, Request);

            return result.Select(x => MakeHAL(x)).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductHAL>> GetByProductId(string id, [FromServices]IProductQueries productQueries)
        {
            var product = await productQueries.GetProductDescription(id);

            CorsTemporarySolution.PerformTemporarily(product, Request);

            return MakeHAL(product);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromForm] Add data, [FromServices] ProductAddHandler handler)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<ProductAddRequest>(data);

                request.NewImageFiles = await data.NewImages.SaveFilesTemporarilyAsync();
                await handler.Handle(request);

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(string id, [FromForm] Edit data, [FromServices] ProductUpdateHandler handler)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<ProductUpdateRequest>(data);

                request.Id = id;
                request.NewImageFiles = await data.NewImages.SaveFilesTemporarilyAsync();

                CorsTemporarySolution.PerformTemporarily(request);

                await handler.Handle(request);

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(string id, [FromServices]ProductRemoveHandler handler)
        {
            await handler.Handle(new ProductRemoveRequest(id));

            return Ok();
        }

        private ProductHAL MakeHAL(ProductDataView product)
        {
            var protocol = Request.IsHttps ? "https" : "http";
            var dest = _mapper.Map<ProductDataView, ProductHAL>(product);

            dest.Links = new
            {
                Edit = this.Url.RouteUrl("getToEditProduct", new { id = dest.Id }, protocol, Request.Host.ToString())
            };

            return dest;
        }
    }
}