﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.PropertyAggregate.Add;
using SecondHand.Application.Catalog.PropertyAggregate.Queries;
using SecondHand.Application.Catalog.PropertyAggregate.Remove;
using SecondHand.Application.Catalog.PropertyAggregate.Update;
using SecondHand.Web.Areas.BackOffice.Models.Properties;

namespace SecondHand.Web.Areas.BackOffice.Controllers.Api
{
    [Area("BackOffice")]
    [Route("backoffice/api/[controller]")]
    [ApiController]
    public class PropertiesController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<IList<PropertyDataView>>> Get([FromServices] IPropertyQueries propertyQueries)
        {
            return await propertyQueries.GetAllProperties();
        }

        [HttpPut("{alias}")]
        public async Task<IActionResult> Edit(string alias, [FromBody] Edit data, [FromServices] PropertyUpdateHandler handler, [FromServices] IMapper mapper)
        {
            var request = mapper.Map<PropertyUpdateRequest>(data);
            request.RequestedAlias = alias;
            await handler.Perform(request);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Edit data, [FromServices] PropertyAddHandler handler, [FromServices] IMapper mapper)
        {
            var request = mapper.Map<PropertyAddRequest>(data);
            await handler.Perform(request);
            return Ok();
        }

        [HttpDelete("{alias}")]
        public async Task<IActionResult> Remove(string alias, [FromServices] PropertyRemoveHandler handler)
        {
            await handler.Perform(new PropertyRemoveRequest { Alias = alias });
            return Ok();
        }
    }
}