﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.ProductTypeAggregate.Add;
using SecondHand.Application.Catalog.ProductTypeAggregate.Queries;
using SecondHand.Application.Catalog.ProductTypeAggregate.Remove;
using SecondHand.Application.Catalog.ProductTypeAggregate.Update;

namespace SecondHand.Web.Areas.BackOffice.Controllers.Api
{
    [Area("BackOffice")]
    [Route("backoffice/api/[controller]")]
    [ApiController]
    //[Authorize]
    public class ProductTypesController : ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<ProductTypeDataView>> Get([FromServices] IProductTypeQueries productTypeQueries)
        {
            return await productTypeQueries.GetAllProductTypes();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] ProductTypeUpdateQuery query, [FromServices] ProductTypeUpdateHandler handler)
        {
            query.ProductTypeId = id;
            await handler.Handle(query);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductTypeAddRequest query, [FromServices] ProductTypeAddHandler handler)
        {
            await handler.Handle(query);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove([FromRoute] ProductTypeRemoveQuery query, [FromServices] ProductTypeRemoveHandler handler)
        {
            await handler.Handle(query);
            return Ok();
        }
    }
}