﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application;
using SecondHand.Application.Catalog.CategoryAggregate.Add;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Application.Catalog.CategoryAggregate.Remove;
using SecondHand.Application.Catalog.CategoryAggregate.Update;
using SecondHand.Web.Areas.BackOffice.Extensions;
using SecondHand.Web.Areas.BackOffice.Models.Categories;

namespace SecondHand.Web.Areas.BackOffice.Controllers
{
    [Area("BackOffice")]
    [Route("backoffice/[controller]")]
    [Authorize]
    public class CategoriesController : Controller
    {
        IMapper _mapper;

        public CategoriesController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpGet("", Name = "getEditCatigoriesIndex")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{id}/[action]", Name = "getToEditCurrentCategory")]
        public async Task<IActionResult> Edit(string id, [FromServices]ICategoryQueries categoryQueries)
        {
            var data = await categoryQueries.GetCategoryDescriptionAsync(id);
            var model = new Edit();
            model.Name = data.Name;
            model.ImagePath = data.ImagePath;
            model.IsNotDisplayed = data.IsNotDisplayed;
            model.CurrentCategoryId = data.Id;

            return View(model);
        }

        [HttpPost("{id}/[action]", Name = "editCurrentCategory")]
        public async Task<IActionResult> Edit(string id, Edit data, [FromServices]CategoryUpdateHandler handler)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<CategoryUpdateRequest>(data);
                request.CategoryId = id;
                request.ImageFile = await data.Image.SaveFileTemporarilyAsync();

                await handler.Handle(request);

                return RedirectToRoute("getToEditCurrentCategory", new { id });
            }

            return BadRequest(ModelState);
        }

        [HttpGet("[action]", Name = "getToAddRootCategory")]
        public IActionResult AddRoot()
        {
            var model = new CreateRoot();
            model.Name = string.Empty;

            return View(model);
        }

        [HttpPost("[action]", Name = "addRootCategory")]
        public async Task<IActionResult> AddRoot(CreateRoot data, [FromServices]CategoryAddHandler handler)
        {
            if (ModelState.IsValid)
            {
                await handler.Handle(_mapper.Map<CategoryAddRequest>(data));

                return RedirectToRoute("getToAddRootCategory");
            }

            return BadRequest(ModelState);
        }

        [HttpGet("{id}/[action]", Name = "getToAddToCurrentCategory")]
        public IActionResult AddToCurrent(string id)
        {
            var model = new CreateAsChild();
            model.Name = string.Empty;
            model.ImagePath = string.Empty;
            model.CurrentCategoryId = id;

            return View(model);
        }

        [HttpPost("{id}/[action]", Name = "addToCurrentCategory")]
        public async Task<IActionResult> AddToCurrent(string id, CreateAsChild data, [FromServices]CategoryAddHandler handler)
        {
            if (ModelState.IsValid)
            {

                var request = _mapper.Map<CategoryAddRequest>(data);
                request.SelectedCategoryId = id;
                request.ImageFile = await data.Image.SaveFileTemporarilyAsync();

                await handler.Handle(request);

                return RedirectToRoute("getToAddToCurrentCategory", new { id });
            }

            return BadRequest(ModelState);
        }

        [HttpPost("{id}/[action]", Name = "removeCategory")]
        public async Task<IActionResult> Remove(string id, [FromServices]CategoryRemoveHandler handler)
        {
            try
            {
                await handler.Handle(new CategoryRemoveRequest(id));
            }
            catch (BusinessException e)
            {
                return StatusCode(409, new { e.Message });
            }

            if (Request.IsAjaxRequest())
            {
                return Json(new { redirectUrl = this.Url.RouteUrl("getEditCatigoriesIndex") });
            }

            return RedirectToRoute("getEditCatigoriesIndex");
        }
    }
}