﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SecondHand.Web.Areas.BackOffice.Controllers
{
    [Area("BackOffice")]
    [Authorize]
    public class HomeController : Controller
    {
        [Route("backoffice")]
        public IActionResult Index()
        {
            return RedirectToRoute("getToEditProductsIndex");
        }
    }
}