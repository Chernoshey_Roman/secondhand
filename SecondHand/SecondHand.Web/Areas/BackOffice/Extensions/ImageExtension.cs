﻿using Microsoft.AspNetCore.Http;
using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Extensions
{
    public static class ImageExtension
    {
        private static readonly Dictionary<string, string> _allowedMIMETypes;

        static ImageExtension()
        {
            _allowedMIMETypes = new Dictionary<string, string>
            {
                { "image/jpeg", "jpg" }
            };
        }

        public static async Task<TempImage> SaveFileTemporarilyAsync(this IFormFile formFile)
        {
            TempImage tempImage = null;

            if (formFile != null)
            {
                Validate(formFile, out string fileExtention);

                var filePath = Path.GetTempFileName();

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await formFile.CopyToAsync(stream);
                }

                tempImage = new TempImage { TempFilePath = filePath, Extention = fileExtention };
            }

            return tempImage;
        }

        public static async Task<IEnumerable<TempImage>> SaveFilesTemporarilyAsync(this IEnumerable<IFormFile> formFiles)
        {
            var result = new List<TempImage>();

            if (formFiles != null)
            {
                foreach (var formFile in formFiles)
                {
                    result.Add(await SaveFileTemporarilyAsync(formFile));
                }
            }            

            return result;            
        }

        private static void Validate(IFormFile formFile, out string fileExtention)
        {
            if (formFile == null || !(formFile.Length > 0) || !_allowedMIMETypes.TryGetValue(formFile.ContentType, out fileExtention))
            {
                throw new ArgumentException($"files {formFile.FileName} is invalid.");
            }
        }
    }
}
