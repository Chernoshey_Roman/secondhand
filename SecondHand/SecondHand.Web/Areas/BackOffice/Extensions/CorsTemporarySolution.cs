﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Update;

namespace SecondHand.Web.Areas.BackOffice.Extensions
{
    public class CorsTemporarySolution
    {
        public static void PerformTemporarily(ProductUpdateRequest request)
        {
            foreach (var image in request.PersistedImages) //TODO: временно пока у картинки есть хост при сохранении нужно его удалять
            {
                image.Path = new UriBuilder(image.Path).Path;
            }
        }

        public static void PerformTemporarily(ProductDataView request, HttpRequest httpRequest)
        {
            foreach (var image in request.Images)
            {
                image.Path = GetFullPath(image.Path, httpRequest);
            }
        }

        public static void PerformTemporarily(List<ProductDataView> result, HttpRequest httpRequest)
        {
            foreach (var product in result)
            {
                foreach (var image in product.Images)
                {                  
                    image.Path = GetFullPath(image.Path, httpRequest);
                }   
            }            
        }

        public static void PerformTemporarily(CategoryQuery category, HttpRequest request)
        {
            category.ImagePath = GetFullPath(category.ImagePath, request);
        }

        private static string GetFullPath(string originPath, HttpRequest httpRequest)
        {
            var protocol = httpRequest.IsHttps ? "https" : "http";
            return new UriBuilder(protocol, httpRequest.Host.Host, httpRequest.Host.Port ?? 80, originPath).ToString();
        }
    }
}
