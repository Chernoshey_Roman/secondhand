﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using ComponentModels = SecondHand.Web.Areas.BackOffice.Models.Components;

namespace SecondHand.Web.Areas.BackOffice.Components
{
    public class CategoryTreeView : ViewComponent
    {
        private ICategoryQueries _query;

        public CategoryTreeView(ICategoryQueries query)
        {
            this._query = query;
        }

        public async Task<IViewComponentResult> InvokeAsync(string selectedCategoryId)
        {
            var categoryTree = await this._query.GetCategoryTree();
            var model = new ComponentModels.CategoryTreeView
            {                
                Nodes = categoryTree.GetLevel(0),
                SelectedCategoryId = selectedCategoryId
            };

            return View(model);
        }
    }
}
