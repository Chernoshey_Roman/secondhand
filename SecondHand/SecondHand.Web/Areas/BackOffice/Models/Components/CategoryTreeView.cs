﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;

namespace SecondHand.Web.Areas.BackOffice.Models.Components
{
    public class CategoryTreeView
    {
        public IEnumerable<CategoryQuery> Nodes { get; set; }
        public string SelectedCategoryId { get; set; }
    }
}
