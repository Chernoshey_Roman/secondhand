﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.Categories
{
    public class CreateRoot
    {
        [Required(ErrorMessage = "Поле \"Название\"  необходимо заполнить")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Не отоброжать")]
        public bool IsNotDisplayed { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Выберите картинку")]
        public IFormFile Image { get; set; }
    }
}
