﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.Categories
{
    public class CreateAsChild
    {
        [BindNever]
        public string ImagePath { get; set; }
        [BindNever]
        public string CurrentCategoryId { get; set; }


        [Required(ErrorMessage = "Поле \"Название\"  необходимо заполнить")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Выберите картинку")]
        public IFormFile Image { get; set; }

        [Display(Name = "Не отоброжать")]
        public bool IsNotDisplayed { get; set; }
    }
}
