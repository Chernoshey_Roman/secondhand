﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SecondHand.Web.Areas.BackOffice.Models.Categories
{
    public class Edit
    {
        [BindNever]
        public string ImagePath { get; set; }
        [BindNever]
        public string CurrentCategoryId { get; set; }


        [Required(ErrorMessage = "Поле \"Название\"  необходимо заполнить")]
        public string Name { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Выберите картинку")]
        public IFormFile Image { get; set; }

        [Display(Name = "Не отоброжать")]
        public bool IsNotDisplayed { get; set; }
    }
}
