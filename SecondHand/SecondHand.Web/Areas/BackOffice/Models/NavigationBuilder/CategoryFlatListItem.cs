﻿using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder
{
    public class CategoryFlatListItem
    {
        public CategoryQuery Item { get; set; }
        public uint Level { get; set; }
    }
}
