﻿using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder
{
    public class CategoryFlatReportVisitor : AbstractVisitor
    {
        public CategoryFlatReportVisitor()
        {
            Report = new List<CategoryFlatListItem>();
        }

        public List<CategoryFlatListItem> Report { get; }

        public override void Visit(CategoryQuery category, uint parentLevel)
        {
            var ownLevel = parentLevel + 1;

            Report.Add(new CategoryFlatListItem { Item = category, Level = ownLevel });

            foreach (var item in category.Child)
            {
                item.Accept(this, ownLevel);
            }
        }

        public override void Visit(CategoryTree categoryTree)
        {
            foreach (var item in categoryTree.GetLevel(0))
            {
                item.Accept(this, 0);
            }
        }
    }
}
