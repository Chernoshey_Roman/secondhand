﻿using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder
{
    public class NavigationBuilder
    {
        private ICategoryQueries _categoryQueries;

        public NavigationBuilder(ICategoryQueries categoryQueries)
        {
            _categoryQueries = categoryQueries;
        }

        public async Task<List<CategoryFlatListItem>> GetCategoryFlatReport()
        {
            var graph = await _categoryQueries.GetCategoryTree();
            var reportBuilder = new CategoryFlatReportVisitor();

            graph.Accept(reportBuilder);

            return reportBuilder.Report;
        }
    }
}
