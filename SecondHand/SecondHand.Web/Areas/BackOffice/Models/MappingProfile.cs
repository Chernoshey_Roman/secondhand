﻿using AutoMapper;
using SecondHand.Application.Catalog.CategoryAggregate.Add;
using SecondHand.Application.Catalog.CategoryAggregate.Update;
using SecondHand.Application.Catalog.ProductAggregate.Add;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Update;
using SecondHand.Application.Catalog.PropertyAggregate.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductDataView, Products.Api.ProductHAL>()
                .ForMember(dest => dest.Links, opt => opt.Ignore());

            CreateMap<ProductDataView, Products.Edit>()
                .ForMember(dest => dest.CategoryOptions, opt => opt.Ignore())
                .ForMember(dest => dest.NewImages, opt => opt.Ignore())
                .ForMember(dest => dest.SelectedCategoryId, opt => opt.MapFrom(src => src.CategoryId))
                .ForMember(dest => dest.PersistedImages, opt => opt.Ignore());

            CreateMap<Products.Edit, ProductUpdateRequest>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.SelectedCategoryId));

            CreateMap<Categories.Edit, CategoryUpdateRequest>()
                .ForMember(dest => dest.ImageFile, opt => opt.Ignore())
                .ForMember(dest => dest.CategoryId, opt => opt.Ignore());

            CreateMap<Products.Add, ProductAddRequest>()
                .ForMember(dest => dest.NewImageFiles, opt => opt.Ignore())
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.SelectedCategoryId));

            CreateMap<Categories.CreateRoot, CategoryAddRequest>()
                .ForMember(dest => dest.ImageFile, opt => opt.Ignore())
                .ForMember(dest => dest.SelectedCategoryId, opt => opt.Ignore());

            CreateMap<Categories.CreateAsChild, CategoryAddRequest>()
                .ForMember(dest => dest.ImageFile, opt => opt.Ignore())
                .ForMember(dest => dest.SelectedCategoryId, opt => opt.Ignore());

            CreateMap<Properties.Edit, PropertyUpdateRequest>();
        }
    }
}
