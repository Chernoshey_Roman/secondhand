﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.BackOffice.Models.Properties
{
    public class Edit
    {
        public string Alias { get; set; }
        public string Label { get; set; }
    }
}
