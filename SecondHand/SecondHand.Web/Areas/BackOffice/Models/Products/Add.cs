﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder;

namespace SecondHand.Web.Areas.BackOffice.Models.Products
{
    public class Add
    {
        [BindNever]
        public IEnumerable<CategoryFlatListItem> CategoryOptions { get; set; }

        [Required(ErrorMessage = "Поле Название должно быть заполнено", AllowEmptyStrings = false)]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Цена")]        
        [Range(0, int.MaxValue, ErrorMessage = "Цена должна быть положительным числом"), DataType(DataType.Currency)]
        [Required(ErrorMessage = "Поле цена должно быть заполнено")]
        public decimal Price { get; set; }

        [Display(Name = "Количество в наличии")]
        [Range(0, int.MaxValue, ErrorMessage = "Количество в наличии должна быть положительным числом")]
        [Required(ErrorMessage = "Поле \"Количество в наличии\" должно быть заполнено")]
        public uint StockBalance { get; set; }

        [Display(Name = "Выберите картинку")]
        public List<IFormFile> NewImages { get; set; }

        [Display(Name = "Pазмер")]
        public string Size { get; set; }

        [Display(Name = "Скрыть товар")]
        public bool IsHidden { get; set; }

        [Display(Name = "Принадлежность к категории")]
        public string SelectedCategoryId { get; set; }        
    }
}
