﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder;

namespace SecondHand.Web.Areas.BackOffice.Models.Products
{
    public class Index
    {
        [BindNever]
        public List<CategoryFlatListItem> CategoryOptions { get; set; }
    }
}
