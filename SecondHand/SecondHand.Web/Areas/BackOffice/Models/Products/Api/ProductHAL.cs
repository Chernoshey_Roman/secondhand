﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SecondHand.Application.Catalog.ProductAggregate.Queries;

namespace SecondHand.Web.Areas.BackOffice.Models.Products.Api
{
    public class ProductHAL : ProductDataView
    {
        public object Links { get; set; }
    }
}
