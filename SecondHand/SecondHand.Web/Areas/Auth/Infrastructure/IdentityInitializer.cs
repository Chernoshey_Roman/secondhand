﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SecondHand.Web.Areas.Auth.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SecondHand.Web.Areas.Auth.Infrastructure
{
    public class IdentityInitializer
    {
        public static async Task InitializeAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            var users = await userManager.Users.ToListAsync();
            var roles = await roleManager.Roles.ToListAsync();

            if (!roles.Exists(x => x.Name == "client"))
            {
                await roleManager.CreateAsync(new ApplicationRole("admin"));
            }

            if (!roles.Exists(x => x.Name == "manager"))
            {
                await roleManager.CreateAsync(new ApplicationRole("manager"));
            }

            if (!roles.Exists(x => x.Name == "admin"))
            {
                await roleManager.CreateAsync(new ApplicationRole("admin"));
            }

            if (!users.Exists(x => x.UserName == "marina@manager.com"))
            {
                ApplicationUser manager = new ApplicationUser { Email = "marina@manager.com", UserName = "marina@manager.com" };
                IdentityResult result = await userManager.CreateAsync(manager, "Marina_9");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(manager, "manager");
                }
            }

            if (!users.Exists(x => x.UserName == "admin@admin.com"))
            {
                ApplicationUser admin = new ApplicationUser { Email = "admin@admin.com", UserName = "admin@admin.com" };
                IdentityResult result = await userManager.CreateAsync(admin, "Admin_9");
                if (result.Succeeded)
                {
                    await userManager.AddToRolesAsync(admin, new[] { "manager", "admin"});
                }
            }
        }
    }
}
