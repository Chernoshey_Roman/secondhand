﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using SecondHand.Application.Catalog;
using SecondHand.Application.Catalog.CategoryAggregate;
using SecondHand.Application.Catalog.CategoryAggregate.Add;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Application.Catalog.CategoryAggregate.Remove;
using SecondHand.Application.Catalog.CategoryAggregate.Update;
using SecondHand.Application.Catalog.ProductAggregate;
using SecondHand.Application.Catalog.ProductAggregate.Add;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
using SecondHand.Application.Catalog.ProductAggregate.Remove;
using SecondHand.Application.Catalog.ProductAggregate.Update;
using SecondHand.Application.Catalog.ProductTypeAggregate;
using SecondHand.Application.Catalog.ProductTypeAggregate.Add;
using SecondHand.Application.Catalog.ProductTypeAggregate.Queries;
using SecondHand.Application.Catalog.ProductTypeAggregate.Remove;
using SecondHand.Application.Catalog.ProductTypeAggregate.Update;
using SecondHand.Application.Catalog.PropertyAggregate;
using SecondHand.Application.Catalog.PropertyAggregate.Commands.Add;
using SecondHand.Application.Catalog.PropertyAggregate.Commands.Remove;
using SecondHand.Application.Catalog.PropertyAggregate.Commands.Update;
using SecondHand.Application.Catalog.PropertyAggregate.Queries;
using SecondHand.Infrastructure;
using SecondHand.Infrastructure.Catalog.CategoryAggregate;
using SecondHand.Infrastructure.Catalog.CategoryAggregate.Queries;
using SecondHand.Infrastructure.Catalog.ProductAggregate;
using SecondHand.Infrastructure.Catalog.ProductAggregate.Queries;
using SecondHand.Infrastructure.Catalog.ProductTypeAggregate;
using SecondHand.Infrastructure.Catalog.ProductTypeAggregate.Queries;
using SecondHand.Infrastructure.Catalog.PropertyAggregate;
using SecondHand.Infrastructure.Catalog.PropertyAggregate.Queries;
using SecondHand.Infrastructure.Data;
using SecondHand.Web.Areas.Auth.Data;
using SecondHand.Web.Areas.Auth.Infrastructure;
using SecondHand.Web.Areas.BackOffice.Models.NavigationBuilder;
using SecondHand.Web.Infrastructure;

namespace SecondHand.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<RouteOptions>(options => 
            {
                options.LowercaseUrls = true;
            });

            services.AddDbContext<DataContext>(new DataContextFactory(Configuration.GetConnectionString("StoreStorage"), Configuration.GetValue<string>("DataProvider")).Create);
            services.AddDbContext<IdentityContext>(new DataContextFactory(Configuration.GetConnectionString("AuthStorage"), Configuration.GetValue<string>("DataProvider")).Create);

            services.AddTransient<CategoryAddHandler>();
            services.AddTransient<CategoryRemoveHandler>();
            services.AddTransient<CategoryUpdateHandler>();

            services.AddTransient<ProductAddHandler>();
            services.AddTransient<ProductRemoveHandler>();
            services.AddTransient<ProductUpdateHandler>();

            services.AddTransient<PropertyAddHandler>();
            services.AddTransient<PropertyUpdateHandler>();
            services.AddTransient<PropertyRemoveHandler>();
            services.AddTransient<ProductTypeUpdateHandler>();
            services.AddTransient<ProductTypeAddHandler>();
            services.AddTransient<ProductTypeRemoveHandler>();

            services.AddTransient<NavigationBuilder>();
            services.AddTransient<IFileManager, FileManager>();

            services.AddTransient<ICategoryQueries, CategoryQueries>();
            services.AddTransient<IProductQueries, ProductQueries>();
            services.AddTransient<IPropertyQueries, PropertyQueries>();
            services.AddTransient<IProductTypeQueries, ProductTypeQueries>();
            services.AddTransient<IProductTypeRepository, ProductTypeRepository>();

            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IPropertyRepository, PropertyRepository>();
            services.AddScoped<IUrlHelper>(x => {
                var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = x.GetRequiredService<IUrlHelperFactory>();
                return factory.GetUrlHelper(actionContext);
            });

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CategoryImageChangedEventHandler>();
                x.AddConsumer<ProductImageChangedEventHandler>();
                x.AddConsumer<ProductRemovedHandler>();
                x.AddConsumer<ProductAttachedToCategoryEventHandler>();
                x.AddConsumer<CategoryRemovedEventHandler>();
            });

            services.AddSingleton(provider => Bus.Factory.CreateUsingInMemory(cfg =>
            {
                cfg.ReceiveEndpoint("queue_name", ep => { });
            }));
            services.AddSingleton<IBus>(provider => provider.GetRequiredService<IBusControl>());
            services.AddSingleton<IHostedService, BusService>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<IdentityContext>();
            services.ConfigureApplicationCookie(options => 
            {
                options.LoginPath = "/auth/login";
            });
            services.AddCors(); //cors
            services.AddControllersWithViews()
                .AddNewtonsoftJson();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            if (env.IsDevelopment() || env.IsEnvironment("HomeWinDev") || env.IsEnvironment("HomeLinuxDev"))
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            //app.UseCulture();
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("be-BY")
            });
            app.UseStatusCodePages();            
            app.UseStaticFiles();

            var staticFilesPath = FileManager.GetDocsPath();
            Directory.CreateDirectory(staticFilesPath);
            app.UseStaticFiles(new StaticFileOptions
            {                
                FileProvider = new PhysicalFileProvider(staticFilesPath)
            });

            app.UseCookiePolicy();
            app.UseRouting();
            app.UseAuthentication();
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()); // cors
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataContext>();
                DbInitializer.Initialize(context);

                var userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<ApplicationRole>>();
                IdentityInitializer.InitializeAsync(userManager, roleManager).Wait();
            }
        }
    }
}
