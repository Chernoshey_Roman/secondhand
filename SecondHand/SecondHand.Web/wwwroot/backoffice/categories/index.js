﻿(function (window, document, $, undefined) {
    var $treeview = $('.tree-categories__container');

    $treeview
        .on('changed.jstree', function (e, data) {
            if (data.action === 'select_node') {
                document.location = data.node.a_attr.href;
            }            
        })
        .on('ready.jstree', function () {
            $treeview.jstree('open_all');
        })
        .jstree();

})(window, document, jQuery);
