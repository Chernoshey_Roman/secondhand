﻿(function (window, document, $, undefined) {
    $('#remove-category').on('click', function (event) {
        event.preventDefault();
        console.log(event.target.dataset.uri);
        swal({
            title: "Ходите удалить категорию ?",
            text: "",
            icon: "warning",
            buttons: ["Нет", "Да"],
            dangerMode: true,
        })
            .then(function (willDelete) {
                if (willDelete) {
                    return $.post(event.target.dataset.uri);
                } else {
                    return {
                        redirectUrl: null
                    };
                }
            })
            .then(function (data) {
                if (data.redirectUrl) {
                    window.location.href = data.redirectUrl;
                }
            }, function (data) {
                if (data.responseJSON && data.responseJSON.message) {
                    swal({
                        title: "Произошла ошибка",
                        text: data.responseJSON.message,
                        icon: "error",
                        dangerMode: true,
                    })
                }
            });
    });
})(window, document, jQuery);