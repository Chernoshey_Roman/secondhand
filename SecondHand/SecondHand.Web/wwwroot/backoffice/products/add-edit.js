﻿$(document).ready(function () {

    $('#category-sel-product-edit').select2({
        dropdownAutoWidth: true,
        width: "400px",
        templateResult: function (data) {
            // We only really care if there is an element to pull classes from
            if (!data.element) {
                return data.text;
            }

            var $element = $(data.element);

            var $wrapper = $('<span></span>');
            $wrapper.addClass($element[0].className);

            $wrapper.text(data.text);

            return $wrapper;
        }
    });



    $('#remove-product').on('click', function (event) {
        event.preventDefault();
        console.log(event.target.dataset.uri);
        swal({
            title: "Ходите удалить товар ?",
            text: "",
            icon: "warning",
            buttons: ["Нет", "Да"],
            dangerMode: true,
        })
            .then(function (willDelete) {
                if (willDelete) {
                    return $.post(event.target.dataset.uri);
                } else {
                    return {
                        redirectUrl: null
                    };
                }
            })
            .then(function (data) {
                if (data.redirectUrl) {
                    window.location.href = data.redirectUrl;
                }
            });
    });

    if ($.magnificPopup) {
        $('.product-edit__image-link').magnificPopup({
            type: 'image',
            zoom: {
                enabled: true
            }
        });
    }


    (function () {
        // обработка загрузки фото и редактирование загруженных

        var presistedImagesList = [];
        var newImagesToPost = [];
        var desiredImgInput = $('#desired-img-input'); // реальный спрятанный input file


        desiredImgInput.on('change', function () {
            var img, anchor;
            if (!this.files.length) { return; }

            for (var i = 0; i < this.files.length; i++) {
                if (!this.files[i].type.startsWith('image/')) { continue; }
                img = document.createElement("img");
                img.classList.add("preview-imgs__item");
                img.src = window.URL.createObjectURL(this.files[i]);

                $(window).on("beforeunload", (function (obj) {
                    return function () {
                        window.URL.revokeObjectURL(obj);
                    };
                })(img.src));

                anchor = document.createElement("a");
                anchor.classList.add("preview-imgs__item-link");
                anchor.appendChild(img);
                anchor.href = img.src;

                $('.preview-imgs').append(anchor);

                newImagesToPost.push(this.files[i]);
                img = anchor = null;
            }

            $('.preview-imgs__item-link').magnificPopup({
                type: 'image',
                zoom: {
                    enabled: true
                }
            });
        });


        $('#image-fake-btn').on('click', function () { //
            desiredImgInput.click();
        });

        $('#edit-product-form').on('submit', function myfunction(e) {
            e.preventDefault();
            var form = $(this), formData;
            if (!form.valid()) { return; }

            formData = new FormData(this);

            $.each(newImagesToPost, function (index, imageFile) {
                formData.append('NewImages', imageFile);
            });

            $.each(presistedImagesList, function (index, imageFile) {
                formData.append('PersistedImages[' + index + '].path', imageFile.path);
                formData.append('PersistedImages[' + index + '].hasToBeRemoved', imageFile.hasToBeRemoved);
                formData.append('PersistedImages[' + index + '].isPreview', imageFile.isPreview);
            });

            //formData.append('PersistedImages[' + index + '].path', $.param({
            //    path: imageFile.path,
            //    hasToBeRemoved: imageFile.hasToBeRemoved,
            //    isPreview: imageFile.isPreview
            //}));

            $.ajax({
                url: form.attr('action'),
                data: formData,
                type: 'POST',
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                // ... Other options like success and etc
            }).then(function (data) {
                if (data.redirectUrl) {
                    window.location.href = data.redirectUrl;
                }
            });
        });


        $(".product-edit__image-tile").each(function (index, imageTile) {
            var _imageTile = $(imageTile);
            var _image = _imageTile.find(".product-edit__image");
            var _isPrreviewBtn = _imageTile.find(".product-edit__image-add-preview");
            var _hasToBeRemovedBtn = _imageTile.find(".product-edit__image-remove");
            var _presistedImageItem = {
                index: index,
                path: _image.attr("src")
            };

            _presistedImageItem.hasToBeRemoved = false;
            _hasToBeRemovedBtn.data("index", index);
            _hasToBeRemovedBtn.on("click", handleAddToRemove)
            if (_image.data("remove") === "True") {
                _hasToBeRemovedBtn.addClass("product-edit__image-remove_red");
                _presistedImageItem.hasToBeRemoved = true;
            }

            _presistedImageItem.isPreview = false;
            _isPrreviewBtn.data("index", index);
            _isPrreviewBtn.on("click", handleAddToPreview)
            if (_image.data("preview") === "True") {
                _isPrreviewBtn.addClass("product-edit__image-add-preview_green");
                _presistedImageItem.isPreview = true;
            }

            presistedImagesList.push(_presistedImageItem);
        });

        function handleAddToPreview() {
            var btn = $(this);
            var targetElemnt = searchTargetElemnt(this);

            targetElemnt.isPreview = !targetElemnt.isPreview;
            btn.toggleClass("product-edit__image-add-preview_green");
        };

        function handleAddToRemove() {
            var btn = $(this);
            var targetElemnt = searchTargetElemnt(this);

            targetElemnt.hasToBeRemoved = !targetElemnt.hasToBeRemoved;
            btn.toggleClass("product-edit__image-remove_red");
        };

        function searchTargetElemnt(btn) {
            var btn = $(btn);
            var index = btn.data("index");
            return presistedImagesList.find(function (element) {
                return element.index == index;
            });
        };

    })();
});