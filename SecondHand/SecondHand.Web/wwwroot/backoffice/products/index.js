﻿$(document).ready(function () {
    var selectEl = $('#category-sel-product-filter').select2({
        dropdownAutoWidth: true,
        width: "400px",
        templateResult: function (data) {
            // We only really care if there is an element to pull classes from
            if (!data.element) {
                return data.text;
            }

            var $element = $(data.element);
            var $wrapper = $('<span></span>');

            $wrapper.addClass($element[0].className);
            $wrapper.text(data.text);

            return $wrapper;
        }
    })
    .on('select2:select', function (e) {
        fillProducts(e.params.data.id)
    });

    fillProducts(selectEl.select2('data')[0].id);

    function fillProducts(categoryId) {
        $.getJSON('/backoffice/api/products', { categoryId: categoryId }, function (data) {
            if (data && data.length) {
                //установка превью
                $.each(data, function myfunction(index, item) {
                    if (!item.images || !item.images.length) { return; }

                    for (var i = 0; i < item.images.length; i++) {
                        if (item.images[i].isPreview == true) {
                            item.imagePath = item.images[i].path;
                            return;
                        }
                    }
                    item.imagePath = item.images[0].path;
                });
            }
            mustacheRendere(data);
        });
    };

    function mustacheRendere(data) {
        var template = $('#products-list-tmpl-mu').html();
        var rendered = Mustache.render(template, { products: data });
        $('#products-list-container').html(rendered);
    }
});