﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SecondHand.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SecondHand.Web.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ValidationException)
            {
                context.Result = new BadRequestObjectResult(new { ((ValidationException)context.Exception).Message });
                context.ExceptionHandled = true;

                return;
            }
            else if (context.Exception is BusinessException)
            {
                context.Result = new BadRequestObjectResult(new { ((BusinessException)context.Exception).Message });
                context.ExceptionHandled = true;

                return;
            }
            else if (context.Exception is NotFoundException)
            {
                context.Result = new NotFoundObjectResult(new { Message = "Не найдено." });
                context.ExceptionHandled = true;

                return;
            }

            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            context.Result = new ObjectResult(new
            {
                Message = "Произошла неожиданная ошибка на сервере."
            });
        }
    }
}
