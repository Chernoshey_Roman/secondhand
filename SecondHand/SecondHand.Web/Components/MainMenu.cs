﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SecondHand.Web.Models.NavigationBuilder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Html;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;

namespace SecondHand.Web.Components
{
    public class MainMenu : ViewComponent
    {
        private ICategoryQueries _categoryQueries;

        public MainMenu(ICategoryQueries categoryQueries)
        {
            _categoryQueries = categoryQueries;
        }

        public async Task<IHtmlContent> InvokeAsync(Guid selectedCategoryId)
        {
            var buildr = new HtmlContentBuilder();
            var graph = await _categoryQueries.GetCategoryTree();
            var visitor = new CategoriesMainMenuBuilder(this.Url);
            graph.Accept(visitor);

            buildr.AppendHtml("<ul class=\"main-menu__list\">");
            buildr.AppendHtml($"<li class=\"main-menu__item\"><a href=\"{Url.RouteUrl("home")}\" class=\"main-menu__link\">Главная</a></li>");
            buildr.AppendHtml(visitor.Html);
            buildr.AppendHtml("</ul>");

            return buildr;
        }
    }
}
