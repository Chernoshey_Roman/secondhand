﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class DataContextFactory
    {
        string _connection;
        private string _dataProvider;

        public DataContextFactory(string connection, string dataProvider)
        {
            _connection = connection;
            _dataProvider = dataProvider;
        }

        public void Create(DbContextOptionsBuilder options)
        {
            if (_dataProvider == "MSSQL")
            {
                options.UseSqlServer(_connection);
            }
            else if (_dataProvider == "PGSQL")
            {
                options.UseNpgsql(_connection);
            }
        }
    }
}
