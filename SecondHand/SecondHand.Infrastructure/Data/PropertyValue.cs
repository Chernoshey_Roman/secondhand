﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class PropertyValue
    {
        public string ProductId { get; set; }
        public Guid PropertyId { get; set; }
        public Product Product { get; set; }
        public Property Property { get; set; }
        public string Alias { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public string Value { get; set; }
    }
}
