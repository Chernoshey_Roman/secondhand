﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Application.Catalog.CategoryAggregate.Category, Category>();

            // Product request mapping
            CreateMap<Application.Catalog.ProductAggregate.Product, Product>();
            CreateMap<Category, Application.Catalog.CategoryAggregate.Queries.CategoryQuery>()
                .ForMember(p => p.Child, opt => opt.Ignore())
                .ForMember(p => p.Products, opt => opt.Ignore());

            //Product query mapping            
            CreateMap<Product, Application.Catalog.ProductAggregate.Queries.ProductDataView>();
            CreateMap<PropertyValue, Application.Catalog.ProductAggregate.Queries.ProductValueDataView>()
                .ForMember(p => p.Alias, opt => opt.MapFrom(src => src.Property.Alias))
                .ForMember(p => p.Label, opt => opt.MapFrom(src => src.Property.Label));
            CreateMap<ProductImage, Application.Catalog.ProductAggregate.ProductImageSnapshot>();


            //Properties query mapping
            CreateMap<Property, Application.Catalog.PropertyAggregate.Queries.PropertyDataView>();
            CreateMap<Application.Catalog.PropertyAggregate.Property, Property>();

            //ProductType query mapping
            CreateMap<ProductType, Application.Catalog.ProductTypeAggregate.Queries.ProductTypeDataView>();
            CreateMap<PropertyToProductType, Application.Catalog.ProductTypeAggregate.Queries.PropertyDataView>()
                .ForMember(p => p.Alias, opt => opt.MapFrom(src => src.Property.Alias))
                .ForMember(p => p.Label, opt => opt.MapFrom(src => src.Property.Label));
        }
    }
}
