﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            if (context.Products.Any())
            {
                return;   // DB has been seeded
            }

            var clothesPT = new ProductType
            {
                Id = Guid.NewGuid(),
                Description = "Одежда",
                Properties = new List<PropertyToProductType> {
                    new PropertyToProductType {
                        Property = new Property {
                            Id = Guid.NewGuid(),
                            Alias = "name",
                            Label = "Название"
                        }
                    },
                    new PropertyToProductType {
                        Property = new Property {
                            Id = Guid.NewGuid(),
                            Alias = "size",
                            Label = "Размер"
                        }
                    }
                }
            };

            var videocard = new ProductType
            {
                Id = Guid.NewGuid(),
                Description = "Видеокарта",
                Properties = new List<PropertyToProductType> {
                    new PropertyToProductType {
                        Property = new Property {
                            Id = Guid.NewGuid(),
                            Alias = "fps",
                            Label = "FPS"
                        }
                    },
                    new PropertyToProductType {
                        Property = new Property {
                            Id = Guid.NewGuid(),
                            Alias = "mhz",
                            Label = "Частота"
                        }
                    }
                }
            };

            context.ProductTypes.AddRange(clothesPT, videocard);

            var womenClothing = new Category
            {
                Id = "women-clothing",
                Name = "Женская одежда",
                ImagePath = string.Empty,
                ParentCategory = null
            };
            var menClothing = new Category
            {
                Id = "men-clothing",
                Name = "Мужская одежда",
                ImagePath = string.Empty,
                ParentCategory = null
            };

            context.AddRange(new Category[] { womenClothing, menClothing });

            context.SaveChanges();

            context.Categories.AddRange(new Category[] {
                new Category {
                    Id = "dresses",
                    Name = "Платья",
                    ImagePath = "/images/6-23_Womens_Part1-132_360x (1).jpg",
                    ParentCategory = womenClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "suits",
                    Name = "Кастюмы",
                    ImagePath = "/images/09-29_Womens_Baserange_FINAL-1_1_360x.jpg",
                    ParentCategory = womenClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "coat",
                    Name = "Пальто",
                    ImagePath = "/images/07-29_Acne_51_360x.jpg",
                    ParentCategory = womenClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "trousers",
                    Name = "Брюки",
                    ImagePath = "/images/5-18_Womens-11_360x.jpg",
                    ParentCategory = womenClothing,
                    ProductType = clothesPT
                }
            });

            context.Categories.AddRange(new Category[] {
                new Category {
                    Id = "pants",
                    Name = "Штаны",
                    ImagePath = "/images/08-10_Mens_OurLegacy_FINAL-2_360x.jpg",
                    ParentCategory = menClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "jackets",
                    Name = "Куртки",
                    ImagePath = "/images/02-15_Mens_WIP_286_360x.jpg",
                    ParentCategory = menClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "wacky-pants",
                    Name = "Дурацкие штаны",
                    ImagePath = "/images/02-15_Mens_WIP_215_360x.jpg",
                    ParentCategory = menClothing,
                    ProductType = clothesPT
                },
                new Category {
                    Id = "shirts",
                    Name = "Рубашки",
                    ImagePath = "/images/02-15_Mens_WIP_362_360x.jpg",
                    ParentCategory = menClothing,
                    ProductType = clothesPT
                }
            });            

            context.SaveChanges();

            var singleCategory = context.Categories.Single(b => b.Id == "dresses");

            singleCategory.Products = new List<Product>
            {
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/02-22_Womens-147_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                },
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/09-08_Womens_All_FINAL-94_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                },
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/02-22_Womens-147_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                },
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/09-08_Womens_All_FINAL-94_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                },
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/02-22_Womens-147_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                },
                new Product {
                    Id = Path.GetRandomFileName(),
                    Images = new List<ProductImage>{ new ProductImage { IsPreview = true, Path = "/images/dresses/09-08_Womens_All_FINAL-94_220x.jpg" } },
                    Label = "1Square Dress in Off White",
                    Price = 1m,
                    ProductType = clothesPT,
                    IsHidden = false,
                    Values = new List<PropertyValue> {
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "name"), Value = "1Square Dress in Off White" },
                        new PropertyValue { Property = context.Properties.Single(x => x.Alias == "size"), Value = "XXL" }
                    }
                }
            };

            singleCategory.ProductsAmount = 6;

            context.SaveChanges();
        }
    }
}
