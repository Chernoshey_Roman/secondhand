﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class PropertyToProductType
    {
        public Guid ProductTypeId { get; set; }
        public Guid PropertyId { get; set; }
        public ProductType ProductType { get; set; }
        public Property Property { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultValue { get; set; }
    }
}
