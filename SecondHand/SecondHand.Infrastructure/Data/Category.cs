﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class Category : IEntity<string>
    {
        public string Id { get; set; }
        public string ParentCategoryId { get; set; }
        public Guid? ProductTypeId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public uint ProductsAmount { get; set; }
        public bool IsNotDisplayed { get; set; }
        public Category ParentCategory { get; set; }
        public List<Category> ChildCategories { get; set; }
        public List<Product> Products { get; set; }
        public ProductType ProductType { get; set; }
    }
}
