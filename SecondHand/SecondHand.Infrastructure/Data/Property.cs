﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class Property: IAliasEntity<string>, IComparable<Property>, IEquatable<Property>
    {
        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Label { get; set; }
        public string DefaultValue { get; set; }
        public List<PropertyValue> Values { get; set; }
        public List<PropertyToProductType> ProductTypes { get; set; }

        public int CompareTo(Property other)
        {
            return Alias.CompareTo(other.Alias);
        }

        public bool Equals(Property other)
        {
            return Alias.Equals(other.Alias);
        }
    }
}
