﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class ProductType
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public List<Product> Products { get; set; }
        public List<PropertyToProductType> Properties { get; set; }
    }
}
