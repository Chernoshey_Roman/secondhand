﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    [NotMapped]
    public class ProductImage
    {
        public string Path { get; set; }
        public bool IsPreview { get; set; }
    }
}
