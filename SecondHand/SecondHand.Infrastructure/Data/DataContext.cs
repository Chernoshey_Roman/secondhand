﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Infrastructure.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //PropertyValue
            modelBuilder
                .Entity<PropertyValue>()
                .HasKey(k => new { k.ProductId, k.PropertyId });

            modelBuilder
                .Entity<PropertyValue>()
                .HasOne(p => p.Product)
                .WithMany(p => p.Values)
                .HasForeignKey(p => p.ProductId);

            modelBuilder
                .Entity<PropertyValue>()
                .HasOne(p => p.Property)
                .WithMany(p => p.Values)
                .HasForeignKey(p => p.PropertyId);


            //PropertyToProductType
            modelBuilder
                .Entity<PropertyToProductType>()
                .HasKey(k => new { k.ProductTypeId, k.PropertyId });

            modelBuilder
                .Entity<PropertyToProductType>()
                .HasOne(p => p.ProductType)
                .WithMany(p => p.Properties)
                .HasForeignKey(p => p.ProductTypeId);

            modelBuilder
                .Entity<PropertyToProductType>()
                .HasOne(p => p.Property)
                .WithMany(p => p.ProductTypes)
                .HasForeignKey(p => p.PropertyId);


            //Property
            modelBuilder.Entity<Property>()
                .HasIndex(b => b.Alias)
                .IsUnique();

            modelBuilder.Entity<Property>()
                .HasMany(x => x.Values)
                .WithOne(x => x.Property)
                .HasForeignKey(x => x.PropertyId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Property>()
                .HasMany(x => x.ProductTypes)
                .WithOne(x => x.Property)
                .HasForeignKey(x => x.PropertyId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
