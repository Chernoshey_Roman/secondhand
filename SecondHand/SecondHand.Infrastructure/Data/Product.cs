﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SecondHand.Infrastructure.Data
{
    public class Product : IEntity<string>
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public Guid? ProductTypeId { get; set; }
        public string Label { get; set; }
        public uint StockBalance { get; set; }
        public decimal Price { get; set; }
        public bool IsHidden { get; set; }
        public string ImagesXML { get; set; }
        public Category Category { get; set; }
        public ProductType ProductType { get; set; }
        public List<PropertyValue> Values { get; set; }

        [NotMapped]
        public IEnumerable<ProductImage> Images
        {
            get
            {
                if (string.IsNullOrEmpty(ImagesXML))
                {
                    return new List<ProductImage>();
                }
                else
                {
                    return XElement.Parse(ImagesXML)
                    .Elements()
                    .Select(x => new ProductImage { Path = x.Value, IsPreview = (bool)x.Attribute("is-preview") }).ToList();
                }
            }
            set
            {
                if (value != null)
                {
                    XElement root = new XElement("images");
                    foreach (var item in value)
                    {
                        root.Add(new XElement("image", new XAttribute("is-preview", item.IsPreview), item.Path));
                    }

                    ImagesXML = root.ToString(SaveOptions.DisableFormatting);
                }
            }
        }
    }
}
