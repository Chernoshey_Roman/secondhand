﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SecondHand.Application;
using SecondHand.Application.Catalog.PropertyAggregate;
using SecondHand.Infrastructure.Data;

namespace SecondHand.Infrastructure.Catalog.PropertyAggregate
{
    public class PropertyRepository : IPropertyRepository
    {
        DataContext _db;
        IMapper _mapper;

        public PropertyRepository(DataContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<Application.Catalog.PropertyAggregate.Property> GetByAsync(string alias)
        {
            return CreateProperty(await _db.Properties.SingleAsync(x => x.Alias == alias));
        }

        public async Task<bool> IsNotUniqueAliasAsync(string alias)
        {
            return await _db.Properties.AnyAsync(x => x.Alias == alias);
        }

        public async Task AddAsync(Application.Catalog.PropertyAggregate.Property property)
        {
            var data = _mapper.Map<Data.Property>(property);
            data.Id = CreateNewId();
            _db.Add(data);
            await _db.SaveChangesAsync();
        }

        public async Task SaveAsync(Application.Catalog.PropertyAggregate.Property property)
        {
            var data = _db.ChangeTracker.Entries<Data.Property>().Select(x => x.Entity).Single();
            _mapper.Map(property, data);
            await _db.SaveChangesAsync();
        }

        public async Task RemoveAsync(string alias)
        {
            var data = await _db.Properties.SingleAsync(x => x.Alias == alias);
            if (data == null)
                throw new BusinessException($"{alias} - нет такого свойства");
            _db.Remove(data);
            await _db.SaveChangesAsync();    
        }

        public async Task<bool> CanNotBeRemoved(string alias)
        {
            return await _db.Properties
                .Include(x => x.Values)
                .Include(x => x.ProductTypes)
                .AnyAsync(x => x.Alias == alias && (x.Values.Any() || x.ProductTypes.Any()));
        }

        public async Task<IEnumerable<Application.Catalog.PropertyAggregate.Property>> GetListByAsync(IEnumerable<string> aliases)
        {
            var dataList = await _db.Properties.Where(x => aliases.Contains(x.Alias)).ToListAsync();

            return dataList.Select(CreateProperty);
        }

        public static Application.Catalog.PropertyAggregate.Property CreateProperty(Data.Property data)
        {
            return new Application.Catalog.PropertyAggregate.Property(data.Alias, data.Label);
        }

        private Guid CreateNewId()
        {
            return Guid.NewGuid();
        }

        public Task AddAsync(NumericProperty property)
        {
            throw new NotImplementedException();
        }

        public Task AddAsync(SelectiveProperty property)
        {
            throw new NotImplementedException();
        }

        public Task SaveAsync(NumericProperty property)
        {
            throw new NotImplementedException();
        }

        public Task SaveAsync(SelectiveProperty property)
        {
            throw new NotImplementedException();
        }
    }
}
