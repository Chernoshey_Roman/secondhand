﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecondHand.Infrastructure.Data;
using SecondHand.Application.Catalog.PropertyAggregate.Queries;

namespace SecondHand.Infrastructure.Catalog.PropertyAggregate.Queries
{
    public class PropertyQueries : IPropertyQueries
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public PropertyQueries(DataContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<List<PropertyDataView>> GetAllProperties()
        {
            return await _db.Properties
                .AsNoTracking()
                .Select(x => CreateProperty(x))
                .ToListAsync();
        }

        public PropertyDataView CreateProperty(Data.Property data)
        {
            return _mapper.Map<PropertyDataView>(data);
        }
    }
}
