﻿using AutoMapper;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using SecondHand.Application.Catalog.CategoryAggregate;
using SecondHand.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SecondHand.Application.Catalog;

namespace SecondHand.Infrastructure.Catalog.CategoryAggregate
{
    public class CategoryRepository : ICategoryRepository
    {
        DataContext _db;
        IBus _bus;
        IMapper _mapper;

        public CategoryRepository(DataContext dataContext, IBus bus, IMapper mapper)
        {
            _db = dataContext;
            _bus = bus;
            _mapper = mapper;
        }

        public async Task AddAsync(Application.Catalog.CategoryAggregate.Category category)
        {
            _db.Categories.Add(_mapper.Map(category, new Data.Category()));
            await _db.SaveChangesAsync();
        }

        public async Task<Application.Catalog.CategoryAggregate.Category> GetByAsync(string categoryId)
        {
            var data = await _db.Categories
                .Include(x => x.ChildCategories)
                .SingleAsync(x => x.Id == categoryId);

            return CreateCategory(data);
        }

        public async Task<IEnumerable<Application.Catalog.CategoryAggregate.Category>> GetListByAsync(IEnumerable<string> guids)
        {
            var data = await _db.Categories
                .Where(x => guids.Contains(x.Id))
                .ToArrayAsync();

            return data.Select(CreateCategory).ToArray();
        }

        public string GetNewId()
        {
            return Path.GetRandomFileName();
        }

        public async Task<bool> IsExistAsync(string categoryId)
        {
            return await _db.Categories.AnyAsync(x => x.Id == categoryId);
        }

        public async Task RemoveAsync(Application.Catalog.CategoryAggregate.Category category)
        {
            var data = _db.ChangeTracker.Entries<Data.Category>().Single(x => x.Entity.Id == category.Id).Entity;

            _db.Remove(data);

            await _db.SaveChangesAsync();

            foreach (var @event in category.GetEvents())
            {
                await _bus.Publish(@event, @event.GetType());
            }

            await _bus.Publish(new CategoryRemovedEvent(category.Id, category.ImagePath));
        }

        public async Task SaveAsync(Application.Catalog.CategoryAggregate.Category category)
        {
            AlignPersistedData(category);

            await _db.SaveChangesAsync();

            foreach (var @event in category.GetEvents())
            {
                await _bus.Publish(@event, @event.GetType());
            }
        }

        public Task SaveAsync(IEnumerable<Application.Catalog.CategoryAggregate.Category> category)
        {
            throw new NotImplementedException();
        }

        private Application.Catalog.CategoryAggregate.Category CreateCategory(Data.Category data)
        {
            IEnumerable<Application.Catalog.CategoryAggregate.Category> childs = null;

            if (data.ChildCategories != null)
            {
                childs = data.ChildCategories.Select(x => CreateCategory(x));
            }

            return new Application.Catalog.CategoryAggregate.Category
                (data.Id,
                data.ParentCategoryId,
                Guid.Empty,
                data.Name,
                data.ImagePath,
                data.ProductsAmount,
                data.IsNotDisplayed,
                childs);
        }

        private void AlignPersistedData(Application.Catalog.CategoryAggregate.Category category)
        {
            var newEntityesList = new List<Application.Catalog.CategoryAggregate.Category>();
            newEntityesList.Add(category);
            newEntityesList.AddRange(category.GetChilds());

            var cachedEntitys = _db.ChangeTracker.Entries<Data.Category>().Select(x => x.Entity);
            var listComparer = new ListComparerForUpdate<Data.Category, Application.Catalog.CategoryAggregate.Category, string>(cachedEntitys, newEntityesList);

            _db.Categories.RemoveRange(listComparer.ForRemove);
            _db.Categories.AddRange(listComparer.ForAdd.Select(x => _mapper.Map(x, new Data.Category())));

            foreach (var item in listComparer.Existed)
            {
                _mapper.Map(item, cachedEntitys.Single(x => x.Id == item.Id));
            }
        }
    }
}
