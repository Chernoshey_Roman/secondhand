﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SecondHand.Application;
using SecondHand.Application.Catalog.CategoryAggregate.Queries;
using SecondHand.Infrastructure.Catalog.ProductAggregate.Queries;
using SecondHand.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Infrastructure.Catalog.CategoryAggregate.Queries
{
    public class CategoryQueries : ICategoryQueries
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public CategoryQueries(DataContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<CategoryQuery> GetCategoryDescriptionAsync(string categoryId)
        {
            var data = await this._db.Categories.AsNoTracking().SingleOrDefaultAsync(x => x.Id == categoryId);

            if (data == null)
                throw new NotFoundException();

            return CreateCategoryQuery(data);
        }

        public async Task<CategoryTree> GetCategoryTree()
        {
            string sqlQuary =
            @"WITH CategoryReports 
            AS
            (
            -- Anchor member definition
                SELECT e.*, 0 AS Level, e.Id as Stream
                FROM dbo.Categories AS e
                WHERE e.ParentCategoryId IS NULL
                UNION ALL
            -- Recursive member definition
                SELECT e.*, Level + 1, d.Stream
                FROM dbo.Categories AS e
                INNER JOIN CategoryReports AS d
                    ON e.ParentCategoryId = d.Id
            )
            -- Statement that executes the CTE
            SELECT *
            FROM CategoryReports
            ORDER BY Stream ASC, Level ASC, Id ASC";

            string pgQuary =
            @"WITH RECURSIVE ""CategoryReports"" 
            AS
            (
            -- Anchor member definition
                SELECT e.*, 0 AS ""Level"", e.""Id"" as ""Stream""
                FROM ""Categories"" AS e
                WHERE e.""ParentCategoryId"" IS NULL
                UNION ALL
            -- Recursive member definition
                SELECT e.*, ""Level"" + 1, d.""Stream""
                FROM ""Categories"" AS e
                INNER JOIN ""CategoryReports"" AS d
                    ON e.""ParentCategoryId"" = d.""Id""
            )
            -- Statement that executes the CTE
            SELECT *
            FROM ""CategoryReports""
            ORDER BY ""Stream"" ASC, ""Level"" ASC, ""Id"" ASC";

            IQueryable<Data.Category> quary;

            if (this._db.Database.IsSqlServer())
            {
                quary = _db.Categories.FromSqlRaw(sqlQuary).AsNoTracking();
            }
            else if (this._db.Database.IsNpgsql())
            {
                quary = _db.Categories.FromSqlRaw(pgQuary).AsNoTracking();
            }
            else
            {
                quary = _db.Categories.FromSqlRaw(sqlQuary).AsNoTracking();
            }

            var categories = await quary.ToListAsync();

            var rootModel = new CategoryTree();

            foreach (var item in categories)
            {
                rootModel.AddCategory(CreateCategoryQuery(item));
            }

            return rootModel;
        }

        public async Task<CategoryTree> GetCategoryTreeWithProducts(string categoryId)
        {
            string sqlQuary =
            @"WITH CategoryReports 
            AS
            (
            -- Anchor member definition
                SELECT e.*, 0 AS Level, e.Id as Stream
                FROM dbo.Categories AS e
                WHERE e.Id = {0}
                UNION ALL
            -- Recursive member definition
                SELECT e.*, Level + 1, d.Stream
                FROM dbo.Categories AS e
                INNER JOIN CategoryReports AS d
                    ON e.ParentCategoryId = d.Id
            )
            -- Statement that executes the CTE
            SELECT *
            FROM CategoryReports
            ORDER BY Stream ASC, Level ASC, Id ASC";

            string pgQuary =
            @"WITH RECURSIVE ""CategoryReports"" 
            AS
            (
            -- Anchor member definition
                SELECT e.*, 0 AS ""Level"", e.""Id"" as ""Stream""
                FROM ""Categories"" AS e
                WHERE e.""Id"" = {0}
                UNION ALL
            -- Recursive member definition
                SELECT e.*, ""Level"" + 1, d.""Stream""
                FROM ""Categories"" AS e
                INNER JOIN ""CategoryReports"" AS d
                    ON e.""ParentCategoryId"" = d.""Id""
            )
            -- Statement that executes the CTE
            SELECT *
            FROM ""CategoryReports""
            ORDER BY ""Stream"" ASC, ""Level"" ASC, ""Id"" ASC";

            IQueryable<Data.Category> quary;

            if (this._db.Database.IsSqlServer())
            {
                quary = _db.Categories.FromSqlRaw(sqlQuary, categoryId).AsNoTracking();
            }
            else if (this._db.Database.IsNpgsql())
            {
                quary = _db.Categories.FromSqlRaw(pgQuary, categoryId).AsNoTracking();
            }
            else
            {
                quary = _db.Categories.FromSqlRaw(pgQuary, categoryId).AsNoTracking();
            }


            var categories = await quary.ToListAsync();

            var rootData = categories.First();
            categories.Remove(rootData);

            var categoryTree = new CategoryTree();
            categoryTree.AddCategory(CreateCategoryQuery(rootData));

            foreach (var item in categories)
            {
                categoryTree.AddCategory(CreateCategoryQuery(item));
            }

            var leafIds = categoryTree.GetAllCategories().Select(x => x.Id);

            var products = await this._db.Products
                .AsNoTracking()
                .Where(x => leafIds.Contains(x.CategoryId))
                .Select(x => ProductQueries.CreateProduct(x, _mapper))
                .GroupBy(x => x.CategoryId)
                .ToDictionaryAsync(x => x.Key, x => x.ToArray());

            categoryTree.AddProducts(products);

            return categoryTree;
        }

        private CategoryQuery CreateCategoryQuery(Data.Category data)
        {
            return _mapper.Map<CategoryQuery>(data);
        }
    }
}
