﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecondHand.Infrastructure.Catalog
{
    public class AliasListComparerForUpdate<TOld, TNew, TKey>
    where TOld : IAliasEntity<TKey>
    where TNew : IAliasEntity<TKey>
    {
        public AliasListComparerForUpdate(IEnumerable<TOld> oldList, IEnumerable<TNew> newList)
        {
            var oldOrigin = oldList.Cast<IAliasEntity<TKey>>();
            var newOrigin = newList.Cast<IAliasEntity<TKey>>();
            var comparer = new AliasEntityEqualityComparer<TKey>();

            ForRemove = oldOrigin.Except(newOrigin, comparer).Cast<TOld>();
            ForAdd = newOrigin.Except(oldOrigin, comparer).Cast<TNew>();
            Existed = newOrigin.Intersect(oldOrigin, comparer).Cast<TNew>();
        }

        public IEnumerable<TOld> ForRemove { get; private set; }
        public IEnumerable<TNew> ForAdd { get; private set; }
        public IEnumerable<TNew> Existed { get; private set; }
    }

    class AliasEntityEqualityComparer<TKey> : IEqualityComparer<IAliasEntity<TKey>>
    {
        public bool Equals(IAliasEntity<TKey> x, IAliasEntity<TKey> y)
        {
            return x.Equals(x.Alias);
        }

        public int GetHashCode(IAliasEntity<TKey> obj)
        {
            return obj.Alias.GetHashCode();
        }
    }
}
