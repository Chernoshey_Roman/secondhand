﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecondHand.Infrastructure.Catalog
{
    public class ListComparerForUpdate<TOld, TNew, TKey>
        where TOld : IEntity<TKey>
        where TNew : IEntity<TKey>
    {
        public ListComparerForUpdate(IEnumerable<TOld> oldList, IEnumerable<TNew> newList)
        {
            var oldOrigin = oldList.Cast<IEntity<TKey>>();
            var newOrigin = newList.Cast<IEntity<TKey>>();
            var comparer = new EntityEqualityComparer<TKey>();

            ForRemove = oldOrigin.Except(newOrigin, comparer).Cast<TOld>();
            ForAdd = newOrigin.Except(oldOrigin, comparer).Cast<TNew>();
            Existed = newOrigin.Intersect(oldOrigin, comparer).Cast<TNew>();
        }

        public IEnumerable<TOld> ForRemove { get; private set; }
        public IEnumerable<TNew> ForAdd { get; private set; }
        public IEnumerable<TNew> Existed { get; private set; }
    }

    class EntityEqualityComparer<TKey> : IEqualityComparer<IEntity<TKey>>
    {
        public bool Equals(IEntity<TKey> x, IEntity<TKey> y)
        {
            return x.Equals(x.Id);
        }

        public int GetHashCode(IEntity<TKey> obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}



