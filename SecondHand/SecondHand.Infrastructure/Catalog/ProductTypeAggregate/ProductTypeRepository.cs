﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SecondHand.Application;
using SecondHand.Application.Catalog.ProductTypeAggregate;
using SecondHand.Infrastructure.Catalog.PropertyAggregate;
using SecondHand.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Infrastructure.Catalog.ProductTypeAggregate
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        DataContext _db;

        public ProductTypeRepository(DataContext db)
        {
            _db = db;
        }        

        public async Task<Application.Catalog.ProductTypeAggregate.ProductType> GetByAsync(Guid id)
        {
            var data = await _db.ProductTypes
                .Include(x => x.Properties)
                .ThenInclude(x => x.Property)
                .Where(x => x.Id == id)
                .Select(x => new {
                    ProductType = x,
                    ProductCount = x.Products.Count()
                })
                .SingleOrDefaultAsync();

            if (data == null)
                return null;

            return CreateProductType(data.ProductType, data.ProductCount);
        }

        public Guid GetNewId()
        {
            return Guid.NewGuid();
        }

        public async Task SaveAsync(Application.Catalog.ProductTypeAggregate.ProductType entity)
        {
            var productTypeData = _db.ChangeTracker.Entries<Data.ProductType>().Select(x => x.Entity).Single();
            var productTypePropetiesData = productTypeData.Properties.Select(x => x.Property);
            var allPropetiesData = _db.ChangeTracker.Entries<Property>().Select(x => x.Entity);
            var snapshot = entity.GetSnapshot();
            var comparer = new AliasListComparerForUpdate<Property, Application.Catalog.PropertyAggregate.Property, string>(productTypePropetiesData, snapshot.ProductTypeItems.Select(x => x.Property));

            productTypeData.Description = snapshot.Description;
            productTypeData.Properties.RemoveAll(x => comparer.ForRemove.Contains(x.Property));

            foreach (var item in snapshot.ProductTypeItems)
            {
                var propertyData = allPropetiesData.Single(x => item.Property.Alias == x.Alias);
                if (comparer.ForAdd.Contains(item.Property))
                {
                    var propertyToProductTypeData = new PropertyToProductType();
                    propertyToProductTypeData.IsRequired = item.IsRequired;
                    propertyToProductTypeData.DefaultValue = item.DefaultValue;
                    propertyToProductTypeData.Property = propertyData;
                    productTypeData.Properties.Add(propertyToProductTypeData);
                }
                else if(comparer.Existed.Contains(item.Property))
                {
                    var propertyToProductTypeData = propertyData.ProductTypes.Single(x => x.ProductTypeId == snapshot.ProductTypeId);
                    propertyToProductTypeData.IsRequired = item.IsRequired;
                    propertyToProductTypeData.DefaultValue = item.DefaultValue;
                }
            }

            await _db.SaveChangesAsync();
        }

        public async Task AddAsync(Application.Catalog.ProductTypeAggregate.ProductType type)
        {
            var propetiesData = _db.ChangeTracker.Entries<Data.Property>().Select(x => x.Entity);
            var data = new Data.ProductType();

            data.Id = type.Id;
            data.Description = type.Description;
            data.Properties = type.Properties.Select(prop => new PropertyToProductType
            {
                Property = propetiesData.Single(x => prop.Alias == x.Alias)
            }).ToList();

            _db.Add(data);

            await _db.SaveChangesAsync();
        }

        private Application.Catalog.ProductTypeAggregate.ProductType CreateProductType(Data.ProductType data, int productsCount)
        {
            var properties = data.Properties.Select(x => PropertyRepository.CreateProperty(x.Property));

            return new Application.Catalog.ProductTypeAggregate.ProductType(data.Id, data.Description, productsCount, properties.Select(x => new ProductTypeItemSnapshot { Property = x }));
        }

        public async Task RemoveAsync(Application.Catalog.ProductTypeAggregate.ProductType type)
        {
            var data = _db.ChangeTracker.Entries<Data.ProductType>()
                .Select(x => x.Entity)
                .Where(x => x.Id == type.Id)
                .Single();

            _db.ProductTypes.Remove(data);
            await _db.SaveChangesAsync();
        }
    }
}
