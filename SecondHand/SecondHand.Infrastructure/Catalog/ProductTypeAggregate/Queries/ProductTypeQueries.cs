﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using SecondHand.Infrastructure.Data;
using SecondHand.Application.Catalog.ProductTypeAggregate.Queries;

namespace SecondHand.Infrastructure.Catalog.ProductTypeAggregate.Queries
{
    public class ProductTypeQueries : IProductTypeQueries
    {
        DataContext _db;
        IMapper _mapper;

        public ProductTypeQueries(DataContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProductTypeDataView>> GetAllProductTypes()
        {
            return (await _db.ProductTypes
                .Include(x => x.Properties)
                .ThenInclude(x => x.Property)
                .ToArrayAsync()).Select(_mapper.Map<ProductTypeDataView>);
        }
    }
}
