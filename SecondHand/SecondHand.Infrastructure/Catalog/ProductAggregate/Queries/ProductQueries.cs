﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SecondHand.Application;
using SecondHand.Application.Catalog.ProductAggregate.Queries;
//using SecondHand.Application.Catalog.Product;
//using SecondHand.Application.Catalog.Product.Queries;
using SecondHand.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Infrastructure.Catalog.ProductAggregate.Queries
{
    public class ProductQueries : IProductQueries
    {
        private readonly DataContext _db;
        private readonly IMapper _mapper;

        public ProductQueries(DataContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<List<ProductDataView>> GetAllProductsBy(string categoryId = null)
        {
            var query = this._db.Products
                .Include(p => p.Values)
                    .ThenInclude(p => p.Property)
                .AsNoTracking();

            if (!string.IsNullOrEmpty(categoryId))
            {
                query = query.Where(x => x.CategoryId == categoryId);
            }

            return await query.Select(x => CreateProduct(x, _mapper)).ToListAsync();
        }

        public async Task<ProductDataView> GetProductDescription(string productId)
        {
            var data = await this._db.Products
                .Include(p => p.Values)
                    .ThenInclude(p => p.Property)
                .AsNoTracking()
                .SingleAsync(x => x.Id == productId);

            if (data == null)
            {
                throw new BusinessException("There is no such product " + productId);
            }

            return CreateProduct(data, _mapper);
        }

        public async Task<List<ProductDataView>> GetUncategorizedProducts()
        {
            return await this._db.Products
                .Include(p => p.Values)
                    .ThenInclude(p => p.Property)
               .AsNoTracking()
               .Where(x => x.CategoryId == null)
               .Select(x => CreateProduct(x, _mapper)).ToListAsync();
        }

        public static ProductDataView CreateProduct(Data.Product data, IMapper mapper)
        {
            return mapper.Map<ProductDataView>(data);
        }
    }
}
