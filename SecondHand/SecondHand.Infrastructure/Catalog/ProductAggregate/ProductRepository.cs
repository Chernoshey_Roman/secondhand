﻿using AutoMapper;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using SecondHand.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.EntityFrameworkCore.Storage;
using SecondHand.Application.Catalog.ProductAggregate;

namespace SecondHand.Infrastructure.Catalog.ProductAggregate
{
    public class ProductRepository : IProductRepository
    {
        DataContext _db;
        IBus _bus;
        IMapper _mapper;

        public ProductRepository(DataContext dataContext, IBus bus, IMapper mapper)
        {
            _db = dataContext;
            _bus = bus;
            _mapper = mapper;
        }

        public async Task AddAsync(Application.Catalog.ProductAggregate.Product product)
        {
            var data = _mapper.Map<Data.Product>(product);

            _db.Add(data);

            await _db.SaveChangesAsync();
        }

        public IDbContextTransaction BeginTransaction(IsolationLevel level = IsolationLevel.ReadCommitted)
        {
            return _db.Database.BeginTransaction(level);
        }

        public async Task<Application.Catalog.ProductAggregate.Product> GetByAsync(string productId)
        {
            var data = await _db.Products.SingleAsync(x => x.Id == productId);

            return new Application.Catalog.ProductAggregate.Product
                (data.Id,
                data.CategoryId,
                Guid.Empty,
                data.Price,
                data.StockBalance,
                data.IsHidden);
        }

        public string GetNewId()
        {
            return Path.GetRandomFileName();
        }

        public async Task RemoveAsync(Application.Catalog.ProductAggregate.Product product)
        {
            var data = _db.ChangeTracker.Entries<Data.Product>().Select(x => x.Entity).Single();

            _db.Remove(data);

            await _db.SaveChangesAsync();

            foreach (var @event in product.GetEvents())
            {
                await _bus.Publish(@event, @event.GetType());
            }

            await _bus.Publish(new ProductRemovedEvent(product.Id, product.Images.Select(x => x.Path)));
        }

        public async Task SaveAsync(Application.Catalog.ProductAggregate.Product product)
        {
            var data = _db.ChangeTracker.Entries<Data.Product>().Select(x => x.Entity).Single();

            _mapper.Map(product, data);

            await _db.SaveChangesAsync();

            foreach (var @event in product.GetEvents())
            {
                await _bus.Publish(@event, @event.GetType());
            }
        }
    }
}
