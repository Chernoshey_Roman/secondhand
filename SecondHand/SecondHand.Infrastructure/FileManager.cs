﻿using SecondHand.Application.Catalog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Infrastructure
{
    public class FileManager : IFileManager
    {
        readonly string _storagePath;
        readonly string _imageFolder;

        public FileManager()
        {
            _storagePath = GetDocsPath();
            _imageFolder = "images";
        }

        public async Task<string> CreateAsync(ImageFile imageFile)
        {
            var randomFileName = CreateRandomFileName(imageFile.Type);
            var randomPath = CreateRandomPath(randomFileName);
            var persistencePath = CreatePersistencePath(randomPath);

            using (MemoryStream mStream = new MemoryStream(imageFile.File))
            {
                using (FileStream fs = File.Create(Path.Combine(persistencePath, randomFileName)))
                {
                    await mStream.CopyToAsync(fs);
                    await fs.FlushAsync();
                }
            }

            return CreateWebLikePath(randomFileName, randomPath);
        }

        public async Task RemoveAsync(string imagePath)
        {
            var deletedImagePath = Path.Combine(_storagePath, imagePath.Remove(0, 1).Replace('/', Path.DirectorySeparatorChar));
            await Task.Factory.StartNew(() => File.Delete(deletedImagePath));
            Debug.WriteLine(deletedImagePath + " was deleted.");
        }

        public async Task<string> MoveToPersistentStorage(TempImage image)
        {
            var randomFileName = CreateRandomFileName(image.Extention);
            var randomPath = CreateRandomPath(randomFileName);
            var persistencePath = CreatePersistencePath(randomPath);

            using (FileStream SourceStream = File.Open(image.TempFilePath, FileMode.Open))
            {
                using (FileStream DestinationStream = File.Create(Path.Combine(persistencePath, randomFileName)))
                {
                    await SourceStream.CopyToAsync(DestinationStream);
                }
            }

            await Task.Factory.StartNew(() => File.Delete(image.TempFilePath));

            Debug.WriteLine(image.TempFilePath + " was deleted.");

            return CreateWebLikePath(randomFileName, randomPath);
        }

        public static string GetDocsPath()
        {
            return Path.Combine(Directory.GetParent(Environment.CurrentDirectory).ToString(), "secondhand_docs");
        }

        private string CreateWebLikePath(string fileName, string path)
        {
            return '/' + Path.Combine(_imageFolder, path, fileName).Replace(Path.DirectorySeparatorChar, '/');
        }

        private string CreateRandomFileName(string fileExtention)
        {
            string fileName = Path.GetRandomFileName();
            Random r = new Random();
            fileName = fileName.Replace(".", "");

            return Path.ChangeExtension(fileName, fileExtention);
        }

        private string CreateRandomPath(string fileName)
        {
            var random = new Random();
            return Path.Combine(fileName.Substring(random.Next(0, 9), 2), fileName.Substring(random.Next(0, 9), 2));
        }

        private string CreatePersistencePath(string path)
        {
            var persistencePath = Path.Combine(_storagePath, _imageFolder, path);
            Directory.CreateDirectory(persistencePath);

            return persistencePath;
        }
    }
}
