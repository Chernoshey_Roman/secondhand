﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SecondHand.Application.Catalog.PropertyAggregate;
using SecondHand.Application.Catalog.Shared;

namespace SecondHand.Application.Catalog.ProductTypeAggregate
{
    public class ProductType
    {
        int _productsCount;
        SortedSet<ProductTypeItem> _properties;

        public ProductType(Guid id, string dscription, int productsCount, IEnumerable<ProductTypeItemSnapshot> snapshots = null)
        {
            Id = id;
            Description = dscription;
            _productsCount = productsCount;
            _properties = new SortedSet<ProductTypeItem>();
            if (snapshots != null)
            {
                AddOrUpdateItems(snapshots);
            }
        }

        public Guid Id { get; private set; }
        public string Description { get; private set; }
        public IEnumerable<string> PropertyAliases
        {
            get
            {
                return _properties.Select(x => x.PropertyAlias);
            }
        }
        public bool CanBeRemoved => _productsCount <= 0;

        public void UpdateDscription(string dscription) => Description = dscription;

        public void UpdateProperies(IEnumerable<ProductTypeItemSnapshot> snapshots)
        {
            AddOrUpdateItems(snapshots);
        }

        private void AddOrUpdateItems(IEnumerable<ProductTypeItemSnapshot> snapshots)
        {
            if (snapshots == null || snapshots.Any(x => x == null))
                throw new ArgumentNullException("Список \"Свойств\" или его элемент не должн быть null.");            

            var properties = snapshots.Select(x => new ProductTypeItem(x.PropertyAlias, x.IsRequired, x.DefaultValue));

            _properties.Clear();

            foreach (var property in properties)
            {
                if (!_properties.Add(property))
                    throw new ValidationException("Свойство в наборе свойств товара не уникально.");
            }
        }

        public static ProductType Create(Guid id, string dscription, IEnumerable<ProductTypeItemSnapshot> properties = null)
        {
            return new ProductType(id, dscription, 0, properties);
        }
    }
}
