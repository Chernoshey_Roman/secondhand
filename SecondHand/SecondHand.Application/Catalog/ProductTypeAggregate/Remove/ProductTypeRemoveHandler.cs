﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Remove
{
    public class ProductTypeRemoveHandler
    {
        IProductTypeRepository _repository;
        public ProductTypeRemoveHandler(IProductTypeRepository repository)
        {
            _repository = repository;
        }

        public async Task Handle(ProductTypeRemoveQuery query)
        {
            Guid productTypeId;

            if (!Guid.TryParse(query.Id, out productTypeId))
                throw new ValidationException("Уникальный идентификатор не соответствует формату.");

            var productType = await _repository.GetByAsync(productTypeId);

            if(productType == null)
                throw new NotFoundException();

            if (!productType.CanBeRemoved)
                throw new BusinessException("Типа товара не может быть удален так как на него ссылаются товары.");

            await _repository.RemoveAsync(productType);
        }
    }
}
