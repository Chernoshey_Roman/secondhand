﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Remove
{
    public class ProductTypeRemoveQuery
    {
        public string Id { get; set; }
    }
}
