﻿using SecondHand.Application.Catalog.PropertyAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate
{
    public class ProductTypeItemSnapshot
    {
        public string PropertyAlias { get; set; }
        public bool IsRequired { get; set; }
        public string DefaultValue { get; set; }
    }
}
