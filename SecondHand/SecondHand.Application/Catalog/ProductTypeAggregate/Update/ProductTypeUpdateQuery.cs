﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Update
{
    public class ProductTypeUpdateQuery
    {
        public Guid ProductTypeId { get; set; }
        public string Description { get; set; }
        public IEnumerable<string> Properties { get; set; }
    }
}
