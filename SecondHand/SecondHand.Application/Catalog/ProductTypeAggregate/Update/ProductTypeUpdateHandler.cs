﻿using SecondHand.Application.Catalog.PropertyAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Update
{
    public class ProductTypeUpdateHandler
    {
        IProductTypeRepository _repository;
        IPropertyRepository _propertyRepository;

        public ProductTypeUpdateHandler(IProductTypeRepository repository, IPropertyRepository propertyRepository)
        {
            _repository = repository;
            _propertyRepository = propertyRepository;
        }

        public async Task Handle(ProductTypeUpdateQuery query)
        {
            var typeTask = _repository.GetByAsync(query.ProductTypeId);
            var propertiesTask = _propertyRepository.GetListByAsync(query.Properties);

            await Task.WhenAll(typeTask, propertiesTask);

            var type = typeTask.Result;

            if(type == null)
                throw new NotFoundException();

            type.UpdateDscription(query.Description);
            type.UpdateProperies(propertiesTask.Result?.Select(x => new ProductTypeItemSnapshot { Property = x }));      

            await _repository.SaveAsync(type);
        }
    }
}
