﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate
{
    public class ProductTypeItem
    {
        public ProductTypeItem(string propertyAlias, bool isRequired, string defaultValue =  null)
        {
            if(string.IsNullOrWhiteSpace(propertyAlias))
                throw new ValidationException("Элемент типа продукта должет быть инициализирован свойством.");
            PropertyAlias = propertyAlias;
            IsRequired = isRequired;
            DefaultValue = defaultValue;
        }

        public string PropertyAlias { get; }
        public bool IsRequired { get; }
        public string DefaultValue { get; }

        public override bool Equals(object obj)
        {
            return obj != null && obj is ProductTypeItem && Equals((ProductTypeItem)obj);
        }

        public bool Equals(ProductTypeItem other)
        {
            return other != null && PropertyAlias.Equals(other.PropertyAlias);
        }

        public override int GetHashCode()
        {
            return PropertyAlias.GetHashCode();
        }

        public int CompareTo(ProductTypeItem other)
        {
            return PropertyAlias.CompareTo(other.PropertyAlias);
        }
    }
}
