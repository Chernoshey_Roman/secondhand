﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Add
{
    public class ProductTypeAddRequest
    {
        public string Description { get; set; }
        public IEnumerable<string> Properties { get; set; }
    }
}
