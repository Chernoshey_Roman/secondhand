﻿using SecondHand.Application.Catalog.PropertyAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Add
{
    public class ProductTypeAddHandler
    {
        IProductTypeRepository _repository;
        IPropertyRepository _propertyRepository;

        public ProductTypeAddHandler(IProductTypeRepository repository, IPropertyRepository propertyRepository)
        {
            _repository = repository;
            _propertyRepository = propertyRepository;
        }

        public async Task Handle(ProductTypeAddRequest query)
        {
            var properties = await _propertyRepository.GetListByAsync(query.Properties);
            var newType = ProductType.Create(_repository.GetNewId(), query.Description, properties.Select(x => new ProductTypeItemSnapshot { Property = x }));

            await _repository.AddAsync(newType);
        }
    }
}
