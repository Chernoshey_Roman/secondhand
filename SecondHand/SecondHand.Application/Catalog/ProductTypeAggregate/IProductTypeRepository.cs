﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductTypeAggregate
{
    public interface IProductTypeRepository
    {
        Guid GetNewId();
        Task<ProductType> GetByAsync(Guid id);
        Task AddAsync(ProductType type);
        Task SaveAsync(ProductType type);
        Task RemoveAsync(ProductType type);
    }
}
