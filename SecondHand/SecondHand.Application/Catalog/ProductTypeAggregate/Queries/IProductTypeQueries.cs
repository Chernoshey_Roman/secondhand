﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Queries
{
    public interface IProductTypeQueries
    {
        Task<IEnumerable<ProductTypeDataView>> GetAllProductTypes();
    }
}
