﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Queries
{
    public class PropertyDataView
    {
        public string Alias { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
    }
}
