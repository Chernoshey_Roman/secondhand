﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductTypeAggregate.Queries
{
    public class ProductTypeDataView
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public List<PropertyDataView> Properties { get; set; }
    }
}
