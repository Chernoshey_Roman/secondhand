﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Queries
{
    public interface IProductQueries
    {
        Task<ProductDataView> GetProductDescription(string productId);
        Task<List<ProductDataView>> GetAllProductsBy(string categoryId = null);
        Task<List<ProductDataView>> GetUncategorizedProducts();
    }
}
