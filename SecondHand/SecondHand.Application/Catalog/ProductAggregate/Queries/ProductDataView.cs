﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Queries
{
    public class ProductDataView
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public Guid? ProductTypeId { get; set; }
        public string Label { get; set; }
        public decimal Price { get; set; }
        public uint StockBalance { get; set; }
        public bool IsHidden { get; set; }
        public List<ProductImageSnapshot> Images { get; set; }
        public List<ProductValueDataView> Values { get; set; }

        public bool IsThereProductInStock()
        {
            return this.StockBalance > 0;
        }
    }
}
