﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Queries
{
    public class ProductValueDataView
    {
        public string Alias { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
    }
}
