﻿using SecondHand.Application.Catalog.CategoryAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Add
{
    public class ProductAddHandler
    {
        IProductRepository _productRepository;
        ICategoryRepository _categoryRepository;
        IFileManager _fileManager;

        public ProductAddHandler(IProductRepository productRepository, ICategoryRepository categoryRepository, IFileManager fileManager)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _fileManager = fileManager;
        }

        public async Task Handle(ProductAddRequest request)
        {
            Product product;

            if (!string.IsNullOrEmpty(request.CategoryId) && await _categoryRepository.IsExistAsync(request.CategoryId))
            {
                product = Product.CreateCategorized
                    (_productRepository.GetNewId(),
                    request.CategoryId,
                    Guid.Empty,
                    request.Price,
                    request.StockBalance,
                    request.IsHidden);
            }
            else
            {
                product = Product.CreateNotCategorized
                    (_productRepository.GetNewId(),
                    Guid.Empty,
                    request.Price,
                    request.StockBalance,
                    request.IsHidden);
            }

            foreach (var imageItem in request.NewImageFiles)
            {
                product.AddImage(await _fileManager.MoveToPersistentStorage(imageItem));
            }

            await _productRepository.AddAsync(product);
        }
    }
}
