﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Add
{
    public class ProductAddRequest
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public IEnumerable<TempImage> NewImageFiles { get; set; }
        public string Size { get; set; }
        public uint StockBalance { get; set; }
        public bool IsHidden { get; set; }
        public string CategoryId { get; set; }
    }
}
