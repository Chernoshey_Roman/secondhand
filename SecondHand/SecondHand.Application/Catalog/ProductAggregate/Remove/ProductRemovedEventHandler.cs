﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Remove
{
    public class ProductRemovedHandler : IConsumer<ProductRemovedEvent>
    {
        IFileManager _fileManager;

        public ProductRemovedHandler(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public async Task Consume(ConsumeContext<ProductRemovedEvent> context)
        {
            foreach (var item in context.Message.ProductImages)
            {
                await _fileManager.RemoveAsync(item);
            }
        }
    }
}
