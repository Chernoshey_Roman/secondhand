﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Remove
{
    public class ProductRemoveHandler
    {
        IProductRepository _productRepository;

        public ProductRemoveHandler(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task Handle(ProductRemoveRequest request)
        {
            var product = await _productRepository.GetByAsync(request.RemovedProductId);

            await _productRepository.RemoveAsync(product);
        }
    }
}
