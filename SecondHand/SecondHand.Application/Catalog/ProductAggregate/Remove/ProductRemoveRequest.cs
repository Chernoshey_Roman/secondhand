﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Remove
{
    public class ProductRemoveRequest
    {
        public ProductRemoveRequest(string removedProductId)
        {
            RemovedProductId = removedProductId;
        }
        public string RemovedProductId { get; }
    }
}
