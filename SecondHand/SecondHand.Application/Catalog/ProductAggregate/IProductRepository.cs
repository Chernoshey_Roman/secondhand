﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public interface IProductRepository
    {
        Task<Product> GetByAsync(string productId);
        Task SaveAsync(Product product);
        string GetNewId();
        Task AddAsync(Product product);
        Task RemoveAsync(Product product);
        IDbContextTransaction BeginTransaction(IsolationLevel level);
    }
}
