﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SecondHand.Application.Catalog.Shared;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class Product
    {
        private HashSet<PropertyValue> _propertyValues;

        private HashSet<ProductImage> _images;

        private List<ProductEvent> _events;

        public Product
            (string id, 
            string categoryId, 
            Guid? productTypeId,
            decimal price, 
            uint stockBalance, 
            bool isHidden)
        {
            if(string.IsNullOrEmpty(id))
                throw new ArgumentException("Product id should not be empty");
            Id = id;
            CategoryId = categoryId;
            ProductTypeId = productTypeId;
            if (price < 0)
                throw new ArgumentException("Price should be positive value");
            Price = price;
            StockBalance = stockBalance;
            IsHidden = isHidden;
            _propertyValues = new HashSet<PropertyValue>();
            _images = new HashSet<ProductImage>();
            _events = new List<ProductEvent>();
        }

        internal static Product CreateCategorized
            (string id,
            string categoryId,
            Guid productTypeId,
            decimal price,
            uint stockBalance,
            bool isHidden)
        {
            return new Product(id, categoryId, productTypeId, price, stockBalance, isHidden);
        }

        internal static Product CreateNotCategorized
            (string id,
            Guid productTypeId,
            decimal price,
            uint stockBalance,
            bool isHidden)
        {
            return new Product(id, null, productTypeId, price, stockBalance, isHidden);
        }

        public string Id { get; private set; }
        public string CategoryId { get; private set; }
        public Guid? ProductTypeId { get; private set; }
        public decimal Price { get; private set; }

        internal void SetProductType(ProductTypeSnapshot snapshot, IEnumerable<ValueSnapshot> values = null)
        {
            if (snapshot == null)
            {
                _propertyValues.Clear();
                ProductTypeId = null;
                return;
            }

            var valuesDictioanry = values?.ToDictionary(x => x.Alias, x => x.Value) ?? new Dictionary<string, string>();

            _propertyValues.Clear();

            foreach (var productTypeItem in snapshot.ProductTypeItems)
            {
                string value = productTypeItem.DefaultValue ?? productTypeItem.Property.DefaultValue;
                valuesDictioanry.TryGetValue(productTypeItem.Property.Alias, out value);
                _propertyValues.Add(new PropertyValue(productTypeItem.Property, productTypeItem.IsRequired, value));
            }
        }

        internal void SetValues(IEnumerable<ValueSnapshot> values)
        {
            var valuesDictioanry = values?.ToDictionary(x => x.Alias, x => x.Value) ?? new Dictionary<string, string>();

            foreach (var propertyValue in _propertyValues)
            {
                string value = null;
                valuesDictioanry.TryGetValue(propertyValue.Alias, out value);
                propertyValue.SetValue(value);
            }
        }

        public uint StockBalance { get; private set; }
        public bool IsHidden { get; private set; }

        public IEnumerable<PropertyValue> PropertyValues => _propertyValues;

        internal void UpdateStockBalance(uint stockBalance)
        {
            StockBalance = stockBalance;
        }

        internal void UpdateIsHidden(bool isHidden)
        {
            IsHidden = isHidden;
        }

        internal void UpdatePrice(decimal price)
        {
            Price = price;
        }

        public IEnumerable<ProductImage> Images {
            get { return _images; }
        }

        public void UpdateImages(IEnumerable<ProductImageSnapshot> imageSnapshots)
        {
            if (imageSnapshots == null) throw new ArgumentNullException("List of images should not be null.");

            var incomingImages = imageSnapshots.Select(x => new ProductImage(x.Path, x.IsPreview));
            var removedImages = _images.Except(incomingImages).ToArray();
            foreach (var image in incomingImages)
            {
                AddImage(image);
            }

            foreach (var image in removedImages)
            {
                _images.Remove(image);
            }

            _events.Add(new ProductImageChangedEvent(removedImages.Select(x => x.Path)));
        }        

        public void AddImage(string imagePath, bool isPreview = false)
        {
            AddImage(new ProductImage(imagePath, isPreview));
        }

        private void AddImage(ProductImage image)
        {
            if (!_images.Add(image))
            {
                _images.Remove(image);
                _images.Add(image);
            }
        }

        internal void SetCategory(string newCategoryId)
        {
            if (CategoryId != newCategoryId)
            {
                _events.Add(new ProductAttachedToCategoryEvent(CategoryId, newCategoryId));
                CategoryId = newCategoryId;
            }
        }

        public IEnumerable<ProductEvent> GetEvents()
        {
            return _events;
        }
    }
}
