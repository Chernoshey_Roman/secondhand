﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ProductImageChangedEvent : ProductEvent
    {
        public ProductImageChangedEvent(IEnumerable<string> imagesForRemoval)
        {
            this.ImagesForRemoval = imagesForRemoval;
        }

        public IEnumerable<string> ImagesForRemoval { get; }
    }
}
