﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ValueSnapshot
    {
        public string Alias { get; private set; }
        public string Value { get; private set; }
    }
}
