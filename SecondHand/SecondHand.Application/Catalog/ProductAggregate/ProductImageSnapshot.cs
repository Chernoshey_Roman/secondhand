﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ProductImageSnapshot
    {
        public string Path { get; set; }
        public bool IsPreview { get; set; }
    }
}
