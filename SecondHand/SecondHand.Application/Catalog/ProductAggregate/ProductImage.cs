﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ProductImage : IEquatable<ProductImage>
    {
        public ProductImage(string path, bool isPreview)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentException("path of image should not be null or empty");
            Path = path;
            IsPreview = isPreview;
        }

        public string Path { get; }

        public bool IsPreview { get; }

        public override bool Equals(object obj)
        {
            return obj != null && obj is ProductImage && Equals((ProductImage)obj);
        }

        public bool Equals(ProductImage other)
        {
            return other != null && !string.IsNullOrEmpty(Path) && Path == other.Path;
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(Path)) return 0;
            return Path.GetHashCode();
        }
    }
}
