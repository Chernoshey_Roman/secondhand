﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ProductAttachedToCategoryEvent : ProductEvent
    {
        public ProductAttachedToCategoryEvent(string categoryId, string newCategoryId)
        {
            OldCategoryId = categoryId;
            NewCategoryId = newCategoryId;
        }

        public string OldCategoryId { get; private set; }
        public string NewCategoryId { get; private set; }
    }
}
