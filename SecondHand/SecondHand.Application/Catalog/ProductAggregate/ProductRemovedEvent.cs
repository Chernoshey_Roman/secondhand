﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class ProductRemovedEvent
    {
        public ProductRemovedEvent(string removedProductId, IEnumerable<string> productImages)
        {
            RemovedProductId = removedProductId;
            ProductImages = productImages;
        }

        public string RemovedProductId { get; }
        public IEnumerable<string> ProductImages { get; }
    }
}
