﻿using System;
using System.Collections.Generic;
using System.Text;
using SecondHand.Application.Catalog.PropertyAggregate;

namespace SecondHand.Application.Catalog.ProductAggregate
{
    public class PropertyValue
    {
        public PropertyValue
            (Property property,
            bool isRequired,            
            string value)
        {
            Alias = property.Alias;
            Label = property.Label;
            Property = property;
            IsRequired = isRequired;
            SetValue(value);
        }
        public string Alias { get; }
        public string Label { get; }
        public bool IsRequired { get; }
        public Property Property { get; }
        public string Value { get; private set; }

        internal void SetValue(string value)
        {
            if (IsRequired && value == null)
                throw new ValidationException("Поле помеченное как обязательное не долно быть пустым.");
            Value = value;
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj is PropertyValue && Equals((PropertyValue)obj);
        }

        public bool Equals(PropertyValue other)
        {
            return other != null && Alias == other.Alias;
        }

        public override int GetHashCode()
        {
            return Alias.GetHashCode();
        }
    }
}
