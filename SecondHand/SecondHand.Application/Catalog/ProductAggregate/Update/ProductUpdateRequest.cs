﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Update
{
    public class ProductUpdateRequest
    {
        public string Id { get; set; }
        public Guid? ProductTypeId { get; set; }
        public string CategoryId { get; set; }
        public decimal Price { get; set; }
        public uint StockBalance { get; set; }
        public bool IsHidden { get; set; }
        public IEnumerable<ValueSnapshot> Properties { get; set; }
        public IEnumerable<TempImage> NewImageFiles { get; set; }
        public IEnumerable<PersistedImage> PersistedImages { get; set; }
    }
}
