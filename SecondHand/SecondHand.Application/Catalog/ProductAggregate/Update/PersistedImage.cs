﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.ProductAggregate.Update
{
    public class PersistedImage
    {
        public string Path { get; set; }
        public bool IsPreview { get; set; }
        public bool HasToBeRemoved { get; set; }
    }
}
