﻿using SecondHand.Application.Catalog.CategoryAggregate;
using SecondHand.Application.Catalog.ProductTypeAggregate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Update
{
    public class ProductUpdateHandler
    {
        IProductRepository _productRepository;
        ICategoryRepository _categoryRepository;
        IProductTypeRepository _productTypeRepository;
        IFileManager _fileManager;

        public ProductUpdateHandler
            (IProductRepository productRepository,
            ICategoryRepository categoryRepository,
            IProductTypeRepository productTypeRepository,
            IFileManager fileManager)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _productTypeRepository = productTypeRepository;
            _fileManager = fileManager;
        }

        public async Task Handle(ProductUpdateRequest request)
        {
            using (var transaction = _productRepository.BeginTransaction(IsolationLevel.RepeatableRead))
            {
                var product = await _productRepository.GetByAsync(request.Id);                

                if (request.ProductTypeId != product.ProductTypeId)
                {
                    if (request.ProductTypeId.HasValue)
                    {
                        var productType = await _productTypeRepository.GetByAsync(request.ProductTypeId.Value);
                        var snapshot = productType.GetSnapshot();

                        product.SetProductType(snapshot, request.Properties);                        
                    }
                    else
                    {
                        product.SetProductType(null);
                    }                    
                }

                if (!string.IsNullOrEmpty(request.CategoryId) && await _categoryRepository.IsExistAsync(request.CategoryId))
                {
                    product.SetCategory(request.CategoryId);
                }
                else
                {
                    product.SetCategory(null);
                }

                product.UpdatePrice(request.Price);
                product.UpdateStockBalance(request.StockBalance);
                product.UpdateIsHidden(request.IsHidden);                

                var images = request.PersistedImages
                    .Where(x => x.HasToBeRemoved != true)
                    .Select(x => new ProductImageSnapshot { Path = x.Path, IsPreview = x.IsPreview })
                    .ToList();
                product.UpdateImages(images);

                foreach (var imageItem in request.NewImageFiles)
                {
                    images.Add(new ProductImageSnapshot { Path = await _fileManager.MoveToPersistentStorage(imageItem) });
                }

                await _productRepository.SaveAsync(product);

                transaction.Commit();
            }
        }
    }
}
