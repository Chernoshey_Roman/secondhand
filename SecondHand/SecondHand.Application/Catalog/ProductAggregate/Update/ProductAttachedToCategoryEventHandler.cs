﻿using MassTransit;
using SecondHand.Application.Catalog.CategoryAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Update
{
    public class ProductAttachedToCategoryEventHandler : IConsumer<ProductAttachedToCategoryEvent>
    {
        private ICategoryRepository _categoryRepository;

        public ProductAttachedToCategoryEventHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task Consume(ConsumeContext<ProductAttachedToCategoryEvent> context)
        {
            var guids = (new string[] { context.Message.NewCategoryId, context.Message.OldCategoryId })
                .Where(x => !string.IsNullOrEmpty(x));

            var categories = await _categoryRepository.GetListByAsync(guids);

            if (!string.IsNullOrEmpty(context.Message.NewCategoryId))
            {
                categories.Single(x => x.Id == context.Message.NewCategoryId).IncreaseProductsAmount(1);
            }

            if (!string.IsNullOrEmpty(context.Message.OldCategoryId))
            {
                categories.Single(x => x.Id == context.Message.OldCategoryId).DecreaseProductsAmount(1);
            }

            await _categoryRepository.SaveAsync(categories);
        }
    }
}
