﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.ProductAggregate.Update
{
    public class ProductImageChangedEventHandler : IConsumer<ProductImageChangedEvent>
    {
        IFileManager _fileManager;

        public ProductImageChangedEventHandler(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public async Task Consume(ConsumeContext<ProductImageChangedEvent> context)
        {
            foreach (var item in context.Message.ImagesForRemoval)
            {
                await _fileManager.RemoveAsync(item);
            }
        }
    }
}
