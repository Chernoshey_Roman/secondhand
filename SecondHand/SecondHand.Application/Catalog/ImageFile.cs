﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog
{
    public class ImageFile
    {
        public string Type { get; set; }
        public byte[] File { get; set; }
        public string TempFilePath { get; set; }
    }
}
