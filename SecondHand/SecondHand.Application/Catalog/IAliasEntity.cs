﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog
{
    public interface IAliasEntity<TKey>
    {
        TKey Alias { get; }
    }
}
