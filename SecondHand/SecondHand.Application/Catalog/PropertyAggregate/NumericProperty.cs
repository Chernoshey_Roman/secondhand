﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    internal class NumericProperty : StringProperty
    {
        public NumericProperty(string alias, string label, string defaultValue, string unit) : base(alias, label, defaultValue)
        {
            Unit = unit;
        }

        public string Unit { get; private set; }

        protected override void SetDefaultValue(string defaultValue)
        {
            double @out = default;
            if (!string.IsNullOrWhiteSpace(defaultValue) && !double.TryParse(defaultValue, out @out))
                throw new ValidationException("");

            base.SetDefaultValue(@out.ToString());
        }
    }
}
