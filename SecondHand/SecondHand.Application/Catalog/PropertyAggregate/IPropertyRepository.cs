﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    public interface IPropertyRepository
    {
        Task<Property> GetByAsync(string alias);
        Task<IEnumerable<Property>> GetListByAsync(IEnumerable<string> aliases);
        Task<bool> IsNotUniqueAliasAsync(string alias);
        Task<bool> CanNotBeRemoved(string alias);
        Task AddAsync(Property property);
        Task AddAsync(NumericProperty property);
        Task AddAsync(SelectiveProperty property);
        Task SaveAsync(Property property);
        Task SaveAsync(NumericProperty property);
        Task SaveAsync(SelectiveProperty property);
        Task RemoveAsync(string alias);
    }
}
