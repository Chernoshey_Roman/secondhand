﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    internal class BoolProperty : StringProperty
    {
        internal BoolProperty(string alias, string label, string defaultValue) : base(alias, label, defaultValue)
        {
        }

        protected override void SetDefaultValue(string defaultValue)
        {
            if (!bool.TryParse(defaultValue, out var value))
            {
                throw new ValidationException("Значение по умолнию для свойства не корректно.");
            }

            base.SetDefaultValue(value.ToString());
        }
    }
}
