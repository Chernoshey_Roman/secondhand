﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    internal class SelectiveProperty : StringProperty
    {

        IEnumerable<SelectivePropertyOption> _items;

        public SelectiveProperty(string alias, string label, IEnumerable<SelectivePropertyOption> items, string defaultValue = null)
            : base(alias, label, defaultValue) 
        {
            if (items == null || !items.Any())
                throw new ValidationException("Нет ни одно элемента для выбора во много опциональном свойстве.");
            _items = items;
        }
    }
}
