﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    public class StringProperty : IComparable<StringProperty>, IAliasEntity<string>
    {
        // ^[\w-]{1,30}$ - 30 символов(лат) и тирэ от начала до конца строки
        readonly string aliasPattern = @"^[\w-]{1,30}$";

        protected StringProperty(string alias, string label, string defaultValue)
        {
            Update(alias, label, defaultValue);
        }

        public string Alias { get; private set; }
        public string Label { get; private set; }
        public string DefaultValue { get; private set; }

        public void Update(string alias, string label, string defaultValue)
        {
            SetAlias(alias);
            SetLabel(label);
            SetDefaultValue(defaultValue);
        }

        private void SetAlias(string alias)
        {
            if (string.IsNullOrWhiteSpace(alias))
                throw new BusinessException("Уникальный идентификатор не должент быть пустым");

            if (!Regex.IsMatch(alias, aliasPattern))
                throw new BusinessException("Уникальный идентификатор, используются некорректные символы");

            Alias = alias;
        }

        private void SetLabel(string label)
        {
            if (string.IsNullOrEmpty(label))
                throw new BusinessException("Обозначение не должна быть пустой");

            Label = label;
        }

        protected virtual void SetDefaultValue(string defaultValue)
        {
            DefaultValue = defaultValue;
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj is StringProperty && Equals((StringProperty)obj);
        }

        public bool Equals(StringProperty other)
        {
            return other != null && Alias == other.Alias;
        }

        public override int GetHashCode()
        {
            return Alias.GetHashCode();
        }

        public int CompareTo(StringProperty other)
        {
            return Alias.CompareTo(other.Alias);
        }
    }
}
