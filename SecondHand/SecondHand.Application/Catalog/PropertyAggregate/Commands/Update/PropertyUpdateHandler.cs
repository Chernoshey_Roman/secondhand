﻿using SecondHand.Application.Catalog.PropertyAggregate.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands.Update
{
    public class PropertyUpdateHandler
    {
        IPropertyRepository _propertyRepository;
        public PropertyUpdateHandler(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }

        public async Task Perform(PropertyAddUpdateRequest request)  
        {
            var property = await _propertyRepository.GetByAsync(request.Alias);

            if (property == null)
                throw new ValidationException("Редактируемого свойства не существует");

            property.Update(request.Label, request.DefaultValue);

            if (property is BoolProperty)
                Perform((BoolProperty)property, request);
            else if (property is StringProperty)
                Perform((StringProperty)property, request);
            else if (property is NumericProperty)
                Perform((NumericProperty)property, request);
            else if (property is SelectiveProperty)
                Perform((SelectiveProperty)property, request);
            else
                throw new ValidationException("Некорректный тип у свойства");
        }

        private void Perform(BoolProperty property, PropertyAddUpdateRequest request)
        {
            Property _property = null;

            if (request.PropertyType != PropertyType.Boolean)
            {
                if (request.PropertyType == PropertyType.String)
                    _property = property.ToStringProperty();
                else if(request.PropertyType == PropertyType.Numeric)
                    _property = property.ToNumericProperty(request?.ForNumeric?.Unit);
                else
                    _property = property.ToSelectiveProperty(request?.ForSelective?.Optionas.Select(x => new SelectivePropertyOption(x.Label, x.Value)));
            }
        }

        private void Perform(StringProperty property, PropertyAddUpdateRequest request)
        {

        }

        private void Perform(NumericProperty property, PropertyAddUpdateRequest request)
        {

        }

        private void Perform(SelectiveProperty property, PropertyAddUpdateRequest request)
        {

        }

    }
}
