﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands.Add
{
    public class PropertyAddHandler
    {
        IPropertyRepository _propertyRepository;

        public PropertyAddHandler(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }

        public async Task Perform(PropertyAddUpdateRequest request)
        {
            if (await _propertyRepository.IsNotUniqueAliasAsync(request.Alias))
                throw new BusinessException("Свойство не уникально.");

            if (request.PropertyType == PropertyType.Boolean)
            {
                await _propertyRepository.AddAsync(new BoolProperty(request.Alias, request.Label, request.DefaultValue));
            }
            else if (request.PropertyType == PropertyType.String)
            {
                await _propertyRepository.AddAsync(new StringProperty(request.Alias, request.Label, request.DefaultValue));
            }
            else if (request.PropertyType == PropertyType.Numeric)
            {
                await _propertyRepository.AddAsync(new NumericProperty(request.Alias, request.Label, request?.ForNumeric?.Unit, request.DefaultValue));
            }
            else if (request.PropertyType == PropertyType.Selective)
            {
                var property = new SelectiveProperty(
                    request.Alias,
                    request.Label,
                    request?.ForSelective?.Optionas?.Select(x => new SelectivePropertyOption(x.Label, x.Value)),
                    request.DefaultValue);
                await _propertyRepository.AddAsync(property);
            }
            else
                throw new ValidationException("Тип свойства не указан");
        }
    }
}
