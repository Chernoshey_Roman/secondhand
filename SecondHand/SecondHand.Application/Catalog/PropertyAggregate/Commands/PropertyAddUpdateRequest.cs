﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands
{
    public class PropertyAddUpdateRequest
    {
        public string Alias { get; set; }
        public string Label { get; set; }
        public string DefaultValue { get; set; }
        public PropertyType PropertyType { get; set; }
        public ForNumeric ForNumeric { get; set; }
        public ForSelective ForSelective { get; set; }
    }
}
