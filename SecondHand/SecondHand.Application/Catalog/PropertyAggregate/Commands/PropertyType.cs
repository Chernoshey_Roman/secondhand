﻿namespace SecondHand.Application.Catalog.PropertyAggregate.Commands
{
    public enum PropertyType
    {
        Boolean,
        String,
        Numeric,
        Selective
    }
}