﻿using SecondHand.Application.Catalog.PropertyAggregate.Commands.Add;
using System.Collections.Generic;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands
{
    public class ForSelective
    {
        public IEnumerable<SelectiveOptiona> Optionas { get; set; }
    }
}