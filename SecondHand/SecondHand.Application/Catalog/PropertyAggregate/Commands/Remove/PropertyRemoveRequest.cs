﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands.Remove
{
    public class PropertyRemoveRequest
    {
        public string Alias { get; set; }
    }
}
