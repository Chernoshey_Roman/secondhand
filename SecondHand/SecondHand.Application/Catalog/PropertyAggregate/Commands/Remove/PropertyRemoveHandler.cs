﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.PropertyAggregate.Commands.Remove
{
    public class PropertyRemoveHandler
    {
        IPropertyRepository _repository;
        public PropertyRemoveHandler(IPropertyRepository repository)
        {
            _repository = repository;
        }

        public async Task Perform(PropertyRemoveRequest request)
        {
            if (await _repository.CanNotBeRemoved(request.Alias))
                throw new BusinessException($"Свойство {request.Alias} не может быть удалено.");

            await _repository.RemoveAsync(request.Alias);
        }
    }
}
