﻿namespace SecondHand.Application.Catalog.PropertyAggregate.Commands
{
    public class SelectiveOptiona
    {
        public string Label { get; set; }
        public string Value { get; set; }
    }
}