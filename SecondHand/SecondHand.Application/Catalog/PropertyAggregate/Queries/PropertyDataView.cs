﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate.Queries
{
    public class PropertyDataView
    {
        public string Alias { get; set; }
        public string Label { get; set; }
    }
}
