﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.PropertyAggregate.Queries
{
    public interface IPropertyQueries
    {
        Task<List<PropertyDataView>> GetAllProperties();
    }
}
