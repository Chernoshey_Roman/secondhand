﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    internal enum UnitType
    {
        Millimeters,
        Grams
    }
}
