﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.PropertyAggregate
{
    public class SelectivePropertyOption
    {        
        public SelectivePropertyOption(string label, string value)
        {
            if (string.IsNullOrWhiteSpace(label))
                throw new ValidationException("Текст опции не должн быть пустым.");
            Label = label;

            if (string.IsNullOrWhiteSpace(value))
                throw new ValidationException("Значение опции не должно быть пустым.");
            Value = value;
        }

        public string Label { get; }
        public string Value { get; }
    }
}
