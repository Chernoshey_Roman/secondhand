﻿using SecondHand.Application.Catalog.PropertyAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.Shared
{
    public class ProductTypeItemSnapshot
    {
        public ProductTypeItemSnapshot
            (Property property,
            string defaultValue,
            bool isRequired)
        {
            DefaultValue = defaultValue;
            IsRequired = isRequired;
            Property = property;
        }
        public string DefaultValue { get; }
        public bool IsRequired { get; }
        public Property Property { get; }
    }
}
