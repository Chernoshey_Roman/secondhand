﻿using System;
using System.Collections.Generic;
using System.Text;
using SecondHand.Application.Catalog.PropertyAggregate;

namespace SecondHand.Application.Catalog.Shared
{
    public class ProductTypeSnapshot
    {
        List<ProductTypeItemSnapshot> _properties;

        public ProductTypeSnapshot(Guid productTypeId, string description)
        {
            ProductTypeId = productTypeId;
            Description = description;
            _properties = new List<ProductTypeItemSnapshot>();
        }

        public Guid ProductTypeId { get; }
        public string Description { get; }
        public IEnumerable<ProductTypeItemSnapshot> ProductTypeItems => _properties;
        public void AddProperty(Property property, string defaultValue, bool isRequired)
        {
            _properties.Add(new ProductTypeItemSnapshot(property, defaultValue, isRequired));
        }
    }
}
