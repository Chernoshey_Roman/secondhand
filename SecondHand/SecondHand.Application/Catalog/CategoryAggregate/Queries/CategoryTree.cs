﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SecondHand.Application.Catalog.ProductAggregate.Queries;

namespace SecondHand.Application.Catalog.CategoryAggregate.Queries
{
    public class CategoryTree
    {
        protected List<CategoryQuery> _categories;

        public CategoryTree()
        {
            _categories = new List<CategoryQuery>();
        }

        public void AddCategory(CategoryQuery category)
        {
            if (string.IsNullOrEmpty(category.ParentCategoryId))
            {
                this._categories.Add(category);
            }
            else
            {
                var parentCategory = this.BreadthFirstSearch(category.ParentCategoryId);
                if (parentCategory != null)
                {
                    parentCategory.AddChild(category);
                }
                else
                {
                    this._categories.Add(category);
                }
            }
        }

        public IEnumerable<CategoryQuery> GetLevel(uint level)
        {
            return this._categories.SelectMany(x => x.GetLevel(level));
        }

        public void Accept(AbstractVisitor visitor)
        {
            visitor.Visit(this);
        }

        public IEnumerable<CategoryQuery> GetAllCategories()
        {
            return _categories.SelectMany(x => x.GetAllCategories());
        }

        public IEnumerable<ProductDataView> GetAllProducts()
        {

            return _categories.SelectMany(x => x.GetAllProducts());
        }

        public void AddProducts(Dictionary<string, ProductDataView[]> dictionary)
        {
            foreach (var category in _categories)
            {
                category.AddProducts(dictionary);
            }
        }

        public CategoryQuery FindCategoryById(string categoryId)
        {
            return BreadthFirstSearch(categoryId);
        }

        private CategoryQuery BreadthFirstSearch(string targetId)
        {
            Queue<CategoryQuery> q = new Queue<CategoryQuery>(_categories);
            CategoryQuery result = null;

            while (q.Count > 0)
            {
                CategoryQuery current = q.Dequeue();

                if (current == null)
                    continue;

                if (current.Id == targetId)
                {
                    result = current;
                    break;
                }

                foreach (var item in current.Child)
                {
                    q.Enqueue(item);
                }
            }

            return result;
        }
    }
}
