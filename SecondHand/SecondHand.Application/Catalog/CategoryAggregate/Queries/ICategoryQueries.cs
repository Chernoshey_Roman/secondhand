﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Queries
{
    public interface ICategoryQueries
    {
        Task<CategoryTree> GetCategoryTree();
        Task<CategoryTree> GetCategoryTreeWithProducts(string categoryId);
        Task<CategoryQuery> GetCategoryDescriptionAsync(string categoryId);
    }
}
