﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate.Queries
{
    public abstract class AbstractVisitor
    {
        public abstract void Visit(CategoryQuery category, uint level);
        public abstract void Visit(CategoryTree categoryTree);
    }
}
