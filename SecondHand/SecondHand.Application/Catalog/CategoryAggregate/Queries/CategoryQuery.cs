﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SecondHand.Application.Catalog.ProductAggregate.Queries;


namespace SecondHand.Application.Catalog.CategoryAggregate.Queries
{
    public class CategoryQuery
    {
        public CategoryQuery()            
        {
            Child = new List<CategoryQuery>();
            Products = new List<ProductDataView>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool IsNotDisplayed { get; set; }
        public string ParentCategoryId { get; set; }
        public List<CategoryQuery> Child { get; private set; }
        public List<ProductDataView> Products { get; private set; }

        public virtual void AddChild(CategoryQuery category)
        {
            this.Child.Add(category);
        }

        public IEnumerable<CategoryQuery> GetLevel(uint level)
        {
            if (level == 0)
                return new CategoryQuery[] { this };
            else
                return this.Child.SelectMany(x => x.GetLevel(level - 1));
        }

        public void AddProducts(Dictionary<string, ProductDataView[]> dictionary)
        {
            ProductDataView[] data;

            if (dictionary.Remove(this.Id, out data))
            {
                this.Products.AddRange(data);
            }
            else
            {
                foreach (var item in Child)
                {
                    item.AddProducts(dictionary);
                }
            }
        }

        public virtual IEnumerable<ProductDataView> GetAllProducts()
        {
            var list = new List<ProductDataView>(this.Products);

            list.AddRange(this.Child.SelectMany(x => x.GetAllProducts()));

            return list;
        }

        public virtual void Accept(AbstractVisitor visitor, uint level)
        {
            visitor.Visit(this, level);
        }

        public IEnumerable<CategoryQuery> GetAllCategories()
        {
            return this.Child.SelectMany(x => x.GetAllCategories()).Append(this);
        }
    }
}
