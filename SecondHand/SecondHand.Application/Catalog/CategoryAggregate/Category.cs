﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate
{
    public class Category : IEntity<string>
    {
        private List<Category> _childs;

        private List<CategoryEvent> _events;

        public Category
            (string id,
            string parentCategoryId,
            Guid productTypeId,
            string name,
            string imagePath,
            uint productsAmount,
            bool isNotDisplayed,
            IEnumerable<Category> childCategories = null)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentException("ИД товаро борлжно быть корректным");

            Id = id;
            ParentCategoryId = parentCategoryId;
            ProductTypeId = productTypeId;

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Поле имя не долно быть пустым");

            Name = name;
            ImagePath = imagePath; 
            ProductsAmount = productsAmount;
            IsNotDisplayed = isNotDisplayed;

            _events = new List<CategoryEvent>();
            _childs = childCategories != null? new List<Category>(childCategories) : new List<Category>();        
    }

        public string Id { get; private set; }
        public string ParentCategoryId { get; private set; }
        public Guid ProductTypeId { get; private set; }
        public string Name { get; private set; }
        public string ImagePath { get; private set; }
        public uint ProductsAmount { get; private set; }
        public bool IsNotDisplayed { get; private set; }        

        public static Category CreateNewRoot(
            string id,
            Guid productTypeId,
            string name,
            string imagePath,
            bool isNotDisplayed)
        {
            return new Category
                (id,
                null,
                productTypeId,
                name,
                imagePath,
                uint.MinValue,
                isNotDisplayed);
        }

        public void IncreaseProductsAmount(uint count)
        {
            ProductsAmount += count;
        }
        public void DecreaseProductsAmount(uint count)
        {
            ProductsAmount -= count;
        }
        public IReadOnlyCollection<CategoryEvent> GetEvents()
        {
            return _events.AsReadOnly();
        }
        public void Update(string name, string imagePath, bool isNotDisplayed)
        {
            UpdateImagePath(imagePath);
            this.Name = name;
            this.IsNotDisplayed = isNotDisplayed;
        }
        public void UpdateImagePath(string imagePath)
        {
            if (this.ImagePath != imagePath)
            {
                var oldImagePath = this.ImagePath;
                this.ImagePath = imagePath;
                _events.Add(new CategoryImageChangedEvent(this.Id, oldImagePath, this.ImagePath));
            }
        }
        public bool CanBeDeleted()
        {
            return !_childs.Any() && ProductsAmount == 0;
        }

        public Category AddNewCategoryAsChild(
            string id,
            Guid productTypeId,
            string name,
            string imagePath,
            bool isNotDisplayed
            )
        {
            var category = new Category
                (id,
                this.Id,
                productTypeId,
                name,
                imagePath,
                uint.MinValue,
                isNotDisplayed);

            _childs.Add(category);

            return category;
        }

        public IEnumerable<Category> GetChilds()
        {
            return _childs;
        }
    }
}
