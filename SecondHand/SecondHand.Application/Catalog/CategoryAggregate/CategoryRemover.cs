﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate
{
    public class CategoryRemover : CategoryEvent
    {
        public async Task RemoveCayrgotyAsync(ICategoryRepository repository, Category category)
        {
            if (category.CanBeDeleted())
            {
                await repository.RemoveAsync(category);
            }
            else
            {
                throw new BusinessException("Нельзя удалить категорию т.к. она содержит вложенные категрии и(или) товары.");
            }
        }
    }
}
