﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate
{
    public class CategoryImageChangedEvent : CategoryEvent
    {
        public CategoryImageChangedEvent(string categoryId, string oldImagePath, string newImagePath)
        {
            this.EntityId = categoryId;
            this.OldImagePath = oldImagePath;
            this.NewImagePath = newImagePath;
        }
        public string EntityId { get; private set; }
        public string OldImagePath { get; private set; }
        public string NewImagePath { get; private set; }
    }
}
