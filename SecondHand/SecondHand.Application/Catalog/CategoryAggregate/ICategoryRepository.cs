﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate
{
    public interface ICategoryRepository
    {
        Task<Category> GetByAsync(string categoryId);
        Task<IEnumerable<Category>> GetListByAsync(IEnumerable<string> guids);
        Task AddAsync(Category category);
        Task SaveAsync(Category category);
        Task SaveAsync(IEnumerable<Category> category);
        Task RemoveAsync(Category category);
        string GetNewId();
        Task<bool> IsExistAsync(string categoryId);
    }
}
