﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate
{
    public class CategoryRemovedEvent
    {
        public CategoryRemovedEvent(string removedCategoryId, string imagePath)
        {
            RemovedCategoryId = removedCategoryId;
            ImagePath = imagePath;
        }

        public string RemovedCategoryId { get; }
        public string ImagePath { get; }
    }
}
