﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Remove
{
    public class CategoryRemoveHandler
    {
        ICategoryRepository _categoryRep;

        public CategoryRemoveHandler(ICategoryRepository categoryRepository)
        {
            _categoryRep = categoryRepository;
        }
        public async Task Handle(CategoryRemoveRequest categoryRemoveRequest)
        {
            var category = await _categoryRep.GetByAsync(categoryRemoveRequest.RemovedCategoryId);

            await new CategoryRemover().RemoveCayrgotyAsync(_categoryRep, category);
        }
    }
}
