﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate.Remove
{
    public class CategoryRemoveRequest
    {
        public CategoryRemoveRequest(string removedCategoryId)
        {
            RemovedCategoryId = removedCategoryId;
        }

        public string RemovedCategoryId { get; private set; }
    }
}
