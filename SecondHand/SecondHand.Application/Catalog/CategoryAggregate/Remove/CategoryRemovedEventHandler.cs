﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Remove
{
    public class CategoryRemovedEventHandler : IConsumer<CategoryRemovedEvent>
    {
        IFileManager _fileManager;

        public CategoryRemovedEventHandler(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public async Task Consume(ConsumeContext<CategoryRemovedEvent> context)
        {
            if (!string.IsNullOrEmpty(context.Message.ImagePath))
            {
                await _fileManager.RemoveAsync(context.Message.ImagePath);
            }
            else
            {
                await Task.CompletedTask;
            }
        }
    }
}
