﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate.Update
{
    public class CategoryUpdateRequest
    {
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public TempImage ImageFile { get; set; }
        public bool IsNotDisplayed { get; set; }
    }
}
