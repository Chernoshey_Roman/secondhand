﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Update
{
    public class CategoryImageChangedEventHandler : IConsumer<CategoryImageChangedEvent>
    {
        IFileManager _fileManager;

        public CategoryImageChangedEventHandler(IFileManager fileManager)
        {
            _fileManager = fileManager;
        }

        public async Task Consume(ConsumeContext<CategoryImageChangedEvent> context)
        {
            if (!string.IsNullOrEmpty(context.Message.OldImagePath))
            {
                await _fileManager.RemoveAsync(context.Message.OldImagePath);
            }
            else
            {
                await Task.CompletedTask;
            }
        }
    }
}
