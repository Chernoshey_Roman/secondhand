﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Update
{
    public class CategoryUpdateHandler
    {
        ICategoryRepository _categoryRep;
        IFileManager _fileManager;

        public CategoryUpdateHandler(ICategoryRepository categoryRepository, IFileManager fileManager)
        {
            _categoryRep = categoryRepository;
            _fileManager = fileManager;
        }
        public async Task Handle(CategoryUpdateRequest request)
        {
            var category = await _categoryRep.GetByAsync(request.CategoryId);
            var imagePath = request.ImageFile != null ? await _fileManager.MoveToPersistentStorage(request.ImageFile) : category.ImagePath;
            category.Update(request.Name, imagePath, request.IsNotDisplayed);

            await _categoryRep.SaveAsync(category);
        }
    }
}
