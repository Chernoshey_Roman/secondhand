﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog.CategoryAggregate.Add
{
    public class CategoryAddHandler
    {
        ICategoryRepository _categoryRep;
        IFileManager _fileManager;

        public CategoryAddHandler(ICategoryRepository categoryRepository, IFileManager fileManager)
        {
            _categoryRep = categoryRepository;
            _fileManager = fileManager;
        }

        public async Task Handle(CategoryAddRequest request)
        {
            var newFileName = request.ImageFile != null ? await _fileManager.MoveToPersistentStorage(request.ImageFile) : null;

            if (!string.IsNullOrEmpty(request.SelectedCategoryId)) // Root категория или нет
            {
                var parentCategory = await _categoryRep.GetByAsync(request.SelectedCategoryId);
                //TODO: проверить на нулл

                parentCategory.AddNewCategoryAsChild(_categoryRep.GetNewId(), Guid.Empty, request.Name, newFileName, request.IsNotDisplayed);

                await _categoryRep.SaveAsync(parentCategory);
            }
            else
            {
                var newCategory = Category.CreateNewRoot(_categoryRep.GetNewId(), Guid.Empty, request.Name, newFileName, request.IsNotDisplayed);

                await _categoryRep.AddAsync(newCategory);
            }

            //TODO: нужно добавить обработчик ошибок если при сохранении произошла ошибка и удалить сохраненную картинку 
        }
    }
}
