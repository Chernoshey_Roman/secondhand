﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog.CategoryAggregate.Add
{
    public class CategoryAddRequest
    {
        public string SelectedCategoryId { get; set; }
        public string Name { get; set; }
        public bool IsNotDisplayed { get; set; }
        public TempImage ImageFile { get; set; }
    }
}
