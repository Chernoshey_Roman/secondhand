﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog
{
    public interface IEntity<TId>
    {
        TId Id { get; }
    }
}
