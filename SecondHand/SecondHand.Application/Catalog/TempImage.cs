﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHand.Application.Catalog
{
    public class TempImage
    {
        public string Extention { get; set; }
        public string TempFilePath { get; set; }
    }
}
