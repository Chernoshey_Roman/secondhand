﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecondHand.Application.Catalog
{
    public interface IFileManager
    {
        Task<string> CreateAsync(ImageFile imageFile);
        Task RemoveAsync(string imagePath);
        Task<string> MoveToPersistentStorage(TempImage image);
    }
}
