const DataAccess = {

    getCategoriesForSelect() {
        return  fetch('http://localhost:11574/backoffice/api/categories')
        .then(handleErrors)
        .then(response => response.json())
        .then(data => {
            return preparingСategoriesForSelect(data);
        });
    },

    getProductToEdit(id) {
        return fetch(`http://localhost:11574/backoffice/api/products/${id}`)
        .then(handleErrors)
        .then(response => response.json());
    },

    getCategoryTree() {
      return fetch('http://localhost:11574/backoffice/api/categories')
      .then(handleErrors)
      .then(response => response.json());
    },

    putProductToEdit(id, formData) {
        return fetch(`http://localhost:11574/backoffice/api/products/${id}`, {
          method: "PUT",
          mode: 'cors',
          body: formData
        })
        .then(handleErrors)
        .then((response) => {
          if(!response.ok){
            throw new Error('Что то пошло не так!');
          }
        });
    },

    postProductToAdd(formData) {
      return fetch("http://localhost:11574/backoffice/api/products", {
        method: "POST",
        mode: 'cors',
        body: formData
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    removeProduct(id) {
        return fetch(`http://localhost:11574/backoffice/api/products/${id}`, {
          method: 'DELETE',
          mode: 'cors'
        })
        .then(handleErrors)
        .then((response) => {
          if(!response.ok){
            throw new Error('Что то пошло не так!');
          }
        });
    },

    getCategory(id) {      
      return fetch(`http://localhost:11574/backoffice/api/categories/${id}`)
      .then(handleErrors)
      .then(response => response.json());
    },

    putCategoryToEdit(id, formData) {
      return fetch(`http://localhost:11574/backoffice/api/categories/${id}`, {
          method: "PUT",
          mode: 'cors',
          body: formData
        })
        .then(handleErrors)
        .then((response) => {
          if(!response.ok){
            throw new Error('Что то пошло не так!');
          }
        });
    },

    postCategoryToCreateAsChild(parendId, formData) {
      return fetch(`http://localhost:11574/backoffice/api/categories/${parendId}`, {
        method: "POST",
        mode: 'cors',
        body: formData
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    postCategoryToCreateAsRoot(formData) {
      return fetch("http://localhost:11574/backoffice/api/categories", {
        method: "POST",
        mode: 'cors',
        body: formData
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    deleteCtegory(id) {
      return fetch(`http://localhost:11574/backoffice/api/categories/${id}`, {
          method: 'DELETE',
          mode: 'cors'
        })
        .then(handleErrors)
        .then((response) => {
          if(!response.ok){
            throw new Error('Что то пошло не так!');
          }
        });
    },

    getAllProperties() {
      return fetch(`http://localhost:11574/backoffice/api/properties`)
      .then(handleErrors)
      .then(response => response.json());
    },

    saveProperty(alias, data) {
      return fetch(`http://localhost:11574/backoffice/api/properties/${alias}`, {
          method: "PUT",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          body: JSON.stringify(data)
        })
        .then(handleErrors)
        .then((response) => {
          if(!response.ok){
            throw new Error('Что то пошло не так!');
          }
        });
    },

    addNewProperty(data) {
      return fetch(`http://localhost:11574/backoffice/api/properties`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify(data)
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    removeProperty(alias) {
      return fetch(`http://localhost:11574/backoffice/api/properties/${alias}`, {
        method: 'DELETE',
        mode: 'cors'
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    getAllProductTypes() {
      return fetch(`http://localhost:11574/backoffice/api/producttypes`)
      .then(handleErrors)
      .then(response => response.json());
    },

    saveProductType(id, data) {
      return fetch(`http://localhost:11574/backoffice/api/producttypes/${id}`, {
        method: "PUT",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify(data)
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    addProductType(data) {
      return fetch(`http://localhost:11574/backoffice/api/producttypes`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify(data)
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    },

    removeProductType(typeId) {
      return fetch(`http://localhost:11574/backoffice/api/producttypes/${typeId}`, {
        method: 'DELETE',
        mode: 'cors'
      })
      .then(handleErrors)
      .then((response) => {
        if(!response.ok){
          throw new Error('Что то пошло не так!');
        }
      });
    }
};


function handleErrors(response) {
  if (!response.ok) {
      throw Error(response.json().Message);
  }
  return response;
}


function preparingСategoriesForSelect(data) {

    return (function recursive(categories) {
        return categories.map(item => {
            var result = {};
            if(item.child && item.child.length){
                result.child = recursive(item.child);
            }
            result.label = item.name;
            result.value = item.id;
            return result;
        });
    })(data);
}

export default DataAccess;