const helper = {
    getHashFromString(inputData) {
        var hash = 0, i, chr;
        if (inputData.length === 0) return hash;
        for (i = 0; i < inputData.length; i++) {
          chr   = inputData.charCodeAt(i);
          hash  = ((hash << 5) - hash) + chr;
          hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }
};

export default helper;