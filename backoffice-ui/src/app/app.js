import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';
import LeftMenuPanel from '../components/left-menu-panel';
import ProductsPage from '../pages/products';
import CategoriesPage from '../pages/categories';
import ProductEditPage from '../pages/product-edit';
import ProductAddPage from '../pages/product-add';
import PropertiesPage from '../pages/properties';
import ProductTypesPage from '../pages/product-types';



class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div className="layout__wrapper">
          <LeftMenuPanel className={'left-panel'}/>
          <Switch>
            <Route exact path='/backoffice/products' component={ProductsPage}/>
            <Route exact path='/backoffice/products/add' component={ProductAddPage}/>       
            <Route exact path='/backoffice/products/:id' component={ProductEditPage}/>
            <Route path='/backoffice/categories' component={CategoriesPage}/>
            <Route path='/backoffice/properties' component={PropertiesPage}/>
            <Route path='/backoffice/product-types' component={ProductTypesPage}/>
            <Redirect to="/backoffice/products" />
          </Switch>          
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
