import React, { PureComponent } from 'react';
import helper from '../../../services/helper';

class CategoryNewImage extends PureComponent {

    constructor(props) {
        super(props);
        this._realFileInput = React.createRef();
    }

    openWindowClick = (e) => {
        this._realFileInput.current.click();
    }

    fileListAdd = (e) => {
        this.props.onChange({value: e.target.files[0], valid: true});
    }

    render = () => {
        return (
            <>
                <label className="edit-form__label" htmlFor="NewImages">Выберите картинку</label>
                <div className="edit-form__edit-section edit-form__edit-section_al-r">
                    <button id="image-fake-btn" type="button" className="button button_gr add-photo-btn" onClick={this.openWindowClick}>Добавить изображение</button>
                    <input id="desired-img-input" 
                            type="file"
                            style={{display:'none'}}
                            accept="image/*" 
                            ref={this._realFileInput} 
                            onChange={this.fileListAdd}
                            ></input>
                </div>
            </>
        );
    }
}

export default CategoryNewImage;
