import React, { PureComponent } from 'react'

class CategoryName extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      
    };
  }

  componentDidMount = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  componentDidUpdate = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  onChange = (event) => {
    this.props.onChange({value: event.target.value, valid: this.validate(event.target.value)});
  }

  validate = (value) => {
    
  }

  render = () => {
    const value = this.props.value;
    const onChange = this.onChange;

    return (
      <>
        <label className="edit-form__label" htmlFor="Name">Название</label>
        <input className="edit-form__editable" type="text" id="Name" name="Name" value={value} onChange={onChange} />    
      </>
    );
  }
}

export default CategoryName;
