import React, { PureComponent } from 'react'

class CategoryIsNotDisplayed extends PureComponent {

  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  componentDidUpdate = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  onChange = (event) => {
    this.props.onChange({value: event.target.value, valid: this.validate(event.target.value)});
  }

  validate = (value) => {
    
  }

  render = () => {
    const value = this.props.value;
    const onChange = this.onChange;

    return (
      <>
        <label htmlFor="IsNotDisplayed">Не отоброжать</label>
        <div className="edit-form__edit-section edit-form__edit-section_al-r">
          <input type="checkbox" id="IsNotDisplayed" name="IsNotDisplayed" checked={value} onChange={onChange} />
        </div>        
      </>
    );
  }
}

export default CategoryIsNotDisplayed;
