import React, { PureComponent } from 'react';
import DataAccess from '../../../services/dataAccess';
import CategoryName from '../category-name-fromitem';
import CategoryIsNotDisplayed from '../category-isnotdisplayed-fromitem';
import CategoryNewImage from '../category-new-image';

class CategoryEditForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            category: null,
            wasAttemptToSend: false
        };
    }

    componentDidMount = () => {
        this.currentId = this.props.match.params.id;
        this.getCategory();
    }

    componentDidUpdate = () => {
        if( this.currentId !== this.props.match.params.id) {
            this.currentId = this.props.match.params.id;
            this.getCategory();
        }
    }

    getCategory = () => {
        DataAccess
            .getCategory(this.currentId)
            .then(this.prepareCategoryForEdit)
            .then((data) => {
                this.setState({ category: data });
            });
    }

    prepareCategoryForEdit = (data) => {        
        return {
            name: { value: data.name, valid: true },
            isNotDisplayed: {value: data.isNotDisplayed, valid: true},
            image: { value: null, valid: true },
            imagePath: data.imagePath
        };
    }

    nameOnChange = (data) => {
        this.setState({ category: {...this.state.category, name: data}});
    }

    isNotDisplayedOnChange = (data) => {
        this.setState({ category: {...this.state.category, isNotDisplayed: data}});
    }

    imageOnChange = (data) => {
        this.setState({ category: {...this.state.category, image: data}});
    }

    onSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append("Name", this.state.category.name.value);
        formData.append("IsNotDisplayed", this.state.category.isNotDisplayed.value);
        if(this.state.category.image.value) {
            formData.append("Image", this.state.category.image.value);
        }

        DataAccess.putCategoryToEdit(this.props.match.params.id, formData)
        .then((response) => {
            this.props.history.push("/backoffice/categories");
        }).catch((error) => {
            console.log(error.message);
        });
    }

    onDelete = (event) => {
        event.preventDefault();

        DataAccess.deleteCtegory(this.props.match.params.id)
        .then((response) => {
            this.props.history.push("/backoffice/categories");
        }).catch((error) => {
            console.log(error.message);
        });
    }

    render = () => {
        return (
            !!this.state.category &&
            <form method="post" onSubmit={this.onSubmit}>
                <ul className="edit-form">
                    <li className="edit-form__row">                        
                        <CategoryName value={this.state.category.name.value} onChange={this.nameOnChange} wasAttemptToSend={this.state.wasAttemptToSend}/>
                    </li>
                    <li className="edit-form__row">
                        <img className="edit-form__image" src={this.state.category.imagePath} alt={this.state.category.name.value} />
                    </li>
                    <li className="edit-form__row">
                        <CategoryIsNotDisplayed value={this.state.category.isNotDisplayed.value} onChange={this.isNotDisplayedOnChange} wasAttemptToSend={this.state.wasAttemptToSend} />
                    </li>
                    <li className="edit-form__row">
                        <CategoryNewImage  value={this.state.category.image.value} onChange={this.imageOnChange} wasAttemptToSend={this.state.wasAttemptToSend} />
                    </li>
                    <li className="edit-form__row">
                        <button id="remove-category" className="button button_rd" onClick={this.onDelete}>Удалить</button>
                        <div className="edit-form__edit-section edit-form__edit-section_al-r">
                            <button type="submit" className="button button_gr">Сохранить</button>
                        </div>
                    </li>
                </ul>
            </form>
        );
    }
}

export default CategoryEditForm;