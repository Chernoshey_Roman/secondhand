import React, { PureComponent } from 'react';
import DataAccess from '../../../services/dataAccess';
import CategoryName from '../category-name-fromitem';
import CategoryIsNotDisplayed from '../category-isnotdisplayed-fromitem';
import CategoryNewImage from '../category-new-image';

class CategoryAddRootForm extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            category: this.createBlankCategory(),
            wasAttemptToSend: false
        };
    }

    createBlankCategory = (data) => {        
        return {
            name: { value: "", valid: true },
            isNotDisplayed: {value: false, valid: true},
            image: { value: null, valid: true }
        };
    }

    nameOnChange = (data) => {
        this.setState({ category: {...this.state.category, name: data}});
    }

    isNotDisplayedOnChange = (data) => {
        this.setState({ category: {...this.state.category, isNotDisplayed: data}});
    }

    imageOnChange = (data) => {
        this.setState({ category: {...this.state.category, image: data}});
    }

    onSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData();
        formData.append("Name", this.state.category.name.value);
        formData.append("IsNotDisplayed", this.state.category.isNotDisplayed.value);
        if(this.state.category.image.value) {
            formData.append("Image", this.state.category.image.value);
        }

        DataAccess.postCategoryToCreateAsRoot(formData)
        .then((response) => {
            this.props.history.push("/backoffice/categories");
        }).catch((error) => {
            console.log(error.message);
        });
    }

    render = () => {
        return (
            <form method="post" onSubmit={this.onSubmit}>
                <ul className="edit-form">
                    <li className="edit-form__row">                        
                        <CategoryName value={this.state.category.name.value} onChange={this.nameOnChange} wasAttemptToSend={this.state.wasAttemptToSend}/>
                    </li>
                    <li className="edit-form__row">
                        <CategoryIsNotDisplayed value={this.state.category.isNotDisplayed.value} onChange={this.isNotDisplayedOnChange} wasAttemptToSend={this.state.wasAttemptToSend} />
                    </li>
                    <li className="edit-form__row">
                        <CategoryNewImage  value={this.state.category.image.value} onChange={this.imageOnChange} wasAttemptToSend={this.state.wasAttemptToSend} />
                    </li>
                    <li className="edit-form__row">
                        <div className="edit-form__edit-section edit-form__edit-section_al-r">
                            <button type="submit" className="button button_gr">Сохранить</button>
                        </div>
                    </li>
                </ul>
            </form>
        );
    }
}

export default CategoryAddRootForm;