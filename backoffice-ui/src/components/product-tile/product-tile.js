import React from 'react';
import { Link } from "react-router-dom";
import './product-tile.css';

const ProductTile = ({ product }) => {
    var imagePath;
    var image = product.images.find(image => image.isPreview) || product.images[0];
    if(image) {
        imagePath = image.path;
    } else {
        imagePath = '';
    }    

    return (
        <div key={product.id} className="flex-grid__item product-item">
            <div className="product-item__image-container">                
                <img className="product-item__image" src={imagePath} alt="Square Dress in Off White" />
            </div>
            <div className="product-item__info-section">
                <span className="product-item__name">{product.label}</span>
                <span className="product-item__price">{product.price}</span>
            </div>
            {/* <a className="product-item__edit-link" href={product.links.edit}></a> */}
            <Link to={'/backoffice/products/' + product.id} className="product-item__edit-link"/>
        </div>
    );
};

export default ProductTile;