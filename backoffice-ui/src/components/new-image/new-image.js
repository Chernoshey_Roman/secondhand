import React, { PureComponent } from 'react';
import helper from '../../services/helper';

class NewImage extends PureComponent {

    constructor(props)
    {
        super(props);
        this.state = {
            files: []
        };
        this._realFileInput = React.createRef();
    }

    componentWillUnmount = () => {
        this.state.files.forEach((item) => window.URL.revokeObjectURL(item.objURL));
    }

    openWindowClick = (e) => {
        this._realFileInput.current.click();
    }

    fileListAdd = (e) => {
        var inputlement =  e.target;
        var newFiles = Array.from(inputlement.files).map((file) => { 
            return {
                key: helper.getHashFromString('' + new Date().getTime() + file.name),
                file: file,
                objURL: window.URL.createObjectURL(file) 
            }; 
        });
        this.setState((state) => {
            return { files: [...state.files, ...newFiles] };
        }, () => {
            var imagesForView = this.state.files.map(file => { 
                return { 
                    key: file.key,
                    name: file.file.name,
                    file: file.file,
                    objURL: file.objURL};
            });
            this.props.newImagesListChange(imagesForView);
        });
    }

    render = () => {
        return (
            <>
                <label className="edit-form__label" htmlFor="NewImages">Выберите картинку</label>
                <div className="edit-form__edit-section">
                    <button id="image-fake-btn" type="button" className="button button_gr add-photo-btn" onClick={this.openWindowClick}>Добавить изображение</button>
                    <input id="desired-img-input" 
                            type="file" 
                            multiple="multiple" 
                            style={{display:'none'}}
                            accept="image/*" 
                            ref={this._realFileInput} 
                            onChange={this.fileListAdd}
                            ></input>
                </div>
            </>
        );
    }
}

export default NewImage;
