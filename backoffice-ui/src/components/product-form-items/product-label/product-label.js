import React, { PureComponent } from 'react'

class ProductLabel extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      showEmptyError: false,
    };
  }

  componentDidMount = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  componentDidUpdate = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  onChange = (event) => {
    this.props.onChange({value: event.target.value, valid: this.validate(event.target.value)});
  }

  validate = (value) => {
    var isValid = true;
    var wasAttemptToSend = this.props.wasAttemptToSend;

    if(!value || !value.length) {
      if(wasAttemptToSend) {
        this.setState({showEmptyError: true});
      }      
      isValid = false;
    } else {
      this.setState({showEmptyError: false});
    }

    return isValid;
  }

  render = () => {
    const value = this.props.value;
    const showEmptyError = this.state.showEmptyError;
    const onChange = this.onChange;

    return (
      <>
        <label className="edit-form__label" htmlFor="Name">Название</label>
        <div className="edit-form__edit-section">
          <input className="edit-form__editable edit-form__editable_stretched" type="text" id="Name" name="Name" value={value} onChange={onChange}/>
          { showEmptyError && <span>Поле не должно быть пустым!</span> }
        </div>       
      </>
    );
  }
}

export default ProductLabel;
