import ProductLabel from './product-label/product-label';
import ProductPrice from './product-price/product-price';
import ProductStockBalance from './product-stock-balance/product-stock-balance';
import ProductIsHidden from './product-is-hidden/product-is-hidden';

export { ProductLabel, ProductPrice, ProductStockBalance, ProductIsHidden };