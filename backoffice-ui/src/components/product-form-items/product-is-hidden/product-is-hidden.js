import React, { PureComponent } from 'react'

class ProductIsHidden extends PureComponent {

  constructor(props) {
    super(props);
  }

  onChange = (event) => {
    this.props.onChange(event.target.checked);
  }

  render = () => {
    const value = this.props.value;
    return (
      <>
        <label className="edit-form__label" htmlFor="IsHidden">Скрыть товар</label>
        <div className="edit-form__edit-section">
          <input className="edit-form__editable" type="checkbox" id="IsHidden" name="IsHidden" checked={value} onChange={this.onChange}/>
        </div> 
      </>
    );
  }
}

export default ProductIsHidden;
