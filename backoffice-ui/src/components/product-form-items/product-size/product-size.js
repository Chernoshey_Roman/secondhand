import React, { PureComponent } from 'react'

class ProductSize extends PureComponent {

  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  componentDidUpdate = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  onChange = (event) => {
    this.props.onChange({value: event.target.value, valid: this.validate(event.target.value)});
  }

  validate = (value) => {
    return true;
  }

  render = () => {
    const value = this.props.value;
    const onChange = this.onChange;

    return (

        <>
          <label className="edit-form__label" htmlFor="Size">Pазмер</label>
          <input className="edit-form__editable" type="text" id="Size" name="Size" value={value} onChange={onChange}/>
        </>

    );
  }
}

export default ProductSize;
