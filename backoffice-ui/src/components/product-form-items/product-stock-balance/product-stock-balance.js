import React, { PureComponent } from 'react'

class ProductStockBalance extends PureComponent {

  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  componentDidUpdate = () => {
    this.props.onChange({value: this.props.value, valid: this.validate(this.props.value)});
  }

  onChange = (event) => {
    this.props.onChange({value: event.target.value, valid: this.validate(event.target.value)});
  }

  validate = (value) => {
    return true;
  }

  render = () => {
    const value = this.props.value;
    const onChange = this.onChange;

    return (

        <>
          <label className="edit-form__label" htmlFor="StockBalance">Количество в наличии</label>
          <input className="edit-form__editable" type="number" id="StockBalance" name="StockBalance" value={value} onChange={onChange}/>      
        </>

    );
  }
}

export default ProductStockBalance;
