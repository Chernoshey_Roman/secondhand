import React from 'react';
import './top-panel.css';


const TopPanel = ({ items }) => {
    return (
        <div className="top-panel">
            {items.map(item => <div className="top-panel__item">{item}</div> )}
        </div>
    );
};

export default TopPanel;