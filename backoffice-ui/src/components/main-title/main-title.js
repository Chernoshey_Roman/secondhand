import React from 'react';
import './main-title.css';

const MainTitle = ({children}) => {
    return (
        <h1 className="layout__main-title">{ children }</h1>
    );
};

export default MainTitle;