import React, { PureComponent } from 'react';
import memoizeOne from 'memoize-one';
import helper from '../../services/helper';
import './hierarchical-dropdown.css';

class HierarchicalDropdown extends PureComponent  {

    constructor(props) {
        super(props);
        var defaulItem = this.createDefaultItem();
        this.state = {
          itemWasSelected: false,
          optionsList: [],
          current: defaulItem,
          showDropdown: false,
          memoize: {
            defaulItem: defaulItem,
            dataPreparation: memoizeOne(this.dataPreparation),
            hierarchicalSearchByValue: memoizeOne((value, options) => {
              return this.hierarchicalSearch('value',  value, options);
            })
          }
        };
    }

    static getDerivedStateFromProps(props, state) {
      var preparedOptions = state.memoize.dataPreparation(props.items);
      var changes = {
        optionsList: preparedOptions
      };

      if (!state.itemWasSelected && props.initiallySelectedValue && typeof props.initiallySelectedValue === 'string') {
        let selectetItem = state.memoize.hierarchicalSearchByValue(props.initiallySelectedValue, preparedOptions);
        if (selectetItem) {
          changes.current = selectetItem;
        } else {
          changes.current = state.memoize.defaulItem;
        }
      }

      if(state.itemWasSelected && !state.memoize.hierarchicalSearchByValue(state.current.value, preparedOptions)) {
        changes.current = state.memoize.defaulItem;
      }

      return changes;
    }

    getKey = ({ value, label }) => {
      return helper.getHashFromString(value + label);
    }

    createDefaultItem = () => {
      var result = {
        label: 'Ничего не выбрано',
        value: 'null',
        child: []
      };
      result.key = this.getKey(result);

      return result;
    }    

    componentDidMount = () => {
      document.addEventListener('click', this.handleDocumentClick, false);
    }

    componentWillUnmount = () => {
      document.removeEventListener('click', this.handleDocumentClick, false);
    }

    handleDocumentClick = (e) => {
      if(!this._root.contains(e.target)) {
        this.dropdownVisibilityDisable();
      }
    }

    setRootRef = (element) => {
      this._root = element;
    }

    dropdownVisibilitySwitch = () => {
      this.setState((state) => {
        return {showDropdown: !state.showDropdown };
      });
    }

    dropdownVisibilityDisable = () => {
      this.setState({showDropdown: false});
    }

    selectItemHandler = (event) => {
      event.stopPropagation();
      let item = this.hierarchicalSearch('key', event.currentTarget.dataset.key, this.state.optionsList);    
      if(this.props.onChange && (this.state.current !== item)) {
        if(item === this.state.memoize.defaulItem) {
          this.props.onChange(null);
        } else {
          this.props.onChange(this.prepareToSendChanges(item));
        }
      }
      this.setState({ current: item, itemWasSelected: true });
      this.dropdownVisibilityDisable();
    }

    hierarchicalSearch = (key, value, data) => {
      return (function recursive(data) {
        for (let index = 0; index < data.length; index++) {
          const option = data[index];
          if(option[key] == value) {
            return option;
          }
          if(option.child.length) {
            let result = recursive(option.child);
            if(result) {
              return result;
            }
          }        
        }
      })(data);
    }

    dataPreparation = (data) => {
      let _this = this;
      let recursive = (recursiveData) => {

        return recursiveData.map((item) => {
          var result = {
            key: _this.getKey(item),
            label: item.label,
            value: item.value,
            child: recursive(('child' in item && item.child) || [])
          };

          return result;
        });
      };

      let result = recursive(data || []);
      result.unshift(this.state.memoize.defaulItem);

      return result;
    }

    prepareToSendChanges = (selectItem) => {
      return {
        label: selectItem.label,
        value: selectItem.value
      };
    } 

    renderLevel = (data) => {
      return data.map((item) => {
        var child;
        
        if(item.child.length) {
          child = <ul className="hierarchical-dropdown__item-list">
                    {this.renderLevel(item.child)}
                  </ul>
        } else {
          child = null;
        }
        
        return  <li className="hierarchical-dropdown__item" key={item.key} data-key={item.key} onClick={this.selectItemHandler}>
                  <div className="hierarchical-dropdown__item-text">{item.label}</div>
                  { child }
                </li>
      });
    }

    render = () => {
      var dropdown = null;
      if(this.state.optionsList.length) {
        dropdown = <ul className={"hierarchical-dropdown__list" + (this.state.showDropdown ? " hierarchical-dropdown__list_visible" : "")}>
                      {this.renderLevel(this.state.optionsList)}
                  </ul>;
      }
      return (
        <div className={"hierarchical-dropdown " + ("className" in this.props ? this.props.className : "") } ref={this.setRootRef}>
          <div className="hierarchical-dropdown__placeholder" onClick={this.dropdownVisibilitySwitch}>
            <div className="hierarchical-dropdown__placeholder-text">
              {this.state.current.label}
            </div>
            <div className="hierarchical-dropdown__placeholder-arrow"></div>
          </div>
          { dropdown }
        </div>
      );
    }
}

export default HierarchicalDropdown
