import React from 'react';
import { NavLink } from 'react-router-dom';
import './left-menu-panel.css';


const LeftMenuPanel = ({className}) => {
    return (
        <nav className={className}>
            <header className="user-section">
                <span className="user-section__ava"></span>
                <span className="user-section__name">admin@admin.com</span>
                <a className="user-section__quit"></a>
            </header>
            <ul className="navigation-main">
                <li className="navigation-main__item"><span className="navigation-main__partition">Меню</span></li>
                <li className="navigation-main__item"><NavLink to="/backoffice/properties" className="navigation-main__link" activeClassName="navigation-main__link_active">Свойства товаров</NavLink></li>
                <li className="navigation-main__item"><NavLink to="/backoffice/product-types" className="navigation-main__link" activeClassName="navigation-main__link_active">Типы товаров</NavLink></li>
                <li className="navigation-main__item"><NavLink to="/backoffice/categories" className="navigation-main__link" activeClassName="navigation-main__link_active">Категории</NavLink></li>
                <li className="navigation-main__item"><NavLink to="/backoffice/products" className="navigation-main__link" activeClassName="navigation-main__link_active">Товары</NavLink></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
                <li className="navigation-main__item"><span className="navigation-main__partition">Other</span></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
                <li className="navigation-main__item"><a className="navigation-main__link">---</a></li>
            </ul>
        </nav>
    );
};

export default LeftMenuPanel;