import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './product-types.css';
import DataAccess from '../../services/dataAccess';
import TopPanel from '../../components/top-panel';
import MainTitle from '../../components/main-title';
import ProductTypeItem from './product-type-item';

class ProductTypesPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productTypes: [],
            properties: [],
            newProductType: null
        };
    }

    componentDidMount = () => {
        this.retviveProductTypes();
        this.retviveAllProperties();

    }

    retviveProductTypes = () => {
        return DataAccess
            .getAllProductTypes()
            .then(data => {
                this.setState(state => {
                    return {
                        productTypes: data
                    }
                });
            });
    }

    retviveAllProperties = () => {
        return DataAccess
            .getAllProperties()
            .then(data => {
                this.setState(state => {
                    return {
                        properties: data
                    }
                });
            });
    }

    createNewProductType = () => {
        this.setState(state => {
            return {
                newProductType: {
                    id:"#",
                    description:"",
                    properties:[]
                }
            };
        });
    }

    onUpdate = () => {
        this.retviveProductTypes();
    }

    onAdd = () => {
        this.retviveProductTypes();
        this.setState(state => {
            return {
                newProductType: null
            }
        });        
    }

    removeProductType = (typeId) => {
        DataAccess
        .removeProductType(typeId)
        .then(() => {
            this.retviveProductTypes();
        });        
    }


    render() {
        return (
            <main className="layout__main">
                <MainTitle>Управление типами товаров</MainTitle>
                <TopPanel items={[
                    <button className="button top-panel__button" onClick={this.createNewProductType}>Добавить новый тип товара</button>
                ]} />
                <div className="flex-grid">
                    <div className="flex-grid__item">
                        {this.state.productTypes.map((type) => {
                            return <EditedProductTypeItem type={type} selectableProperties={this.state.properties} onSave={this.onUpdate} removeProductType={this.removeProductType} key={type.id}/>;
                        })}
                        {!!this.state.newProductType && <AddedProductTypeItem type={this.state.newProductType} selectableProperties={this.state.properties} onSave={this.onAdd}/>}
                    </div>                                        
                </div>
            </main>
        );
    }
}



class AddedProductTypeItem extends Component {

    onSave = (data) => {
        delete data.id;
        return DataAccess
        .addProductType(data)
        .then(this.props.onSave);
    }

    render() {
        return <ProductTypeItem {...this.props} startLineEditing={true} onSave={this.onSave}/>
    }
}


class EditedProductTypeItem extends Component {

    onSave = (data) => {
        var id = data.id;
        delete data.id  
        return DataAccess
        .saveProductType(id, data)
        .then(this.props.onSave);
    }

    render() {
        return <ProductTypeItem {...this.props} startLineEditing={false} onSave={this.onSave}/>
    }
}


  
export default ProductTypesPage;