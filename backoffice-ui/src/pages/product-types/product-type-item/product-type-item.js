import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './product-type-item.css';
import HierarchicalDropdown from '../../../components/hierarchical-dropdown';

class ProductTypeItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: null
        };
    }

    componentWillMount = () => {
        if("startLineEditing" in this.props && this.props.startLineEditing) {
            this.startLineEditing();
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevProps.type != this.props.type) {
            this.setState((state) =>{ return {   edit: null   } });
        }
    }

    startLineEditing = () => {
        this.setState(state => {
            var result = {
                edit: JSON.parse(JSON.stringify(this.props.type))
            };

            result.edit.newProperty = null;
            result.edit.addPropertyButtonIsActive = true;

            return result;
        });
    }

    cancelLineEditing = () => {
        this.setState(state => {
            return {
                edit: null
            };
        });
    }

    onChange = (event, field) => {
        var target = event.currentTarget;
        this.setState(state => {
            return {
                edit: {
                    ...state.edit,
                    [field]: target.value
                }
            };
        });
    }

    addProperty = () => {
        this.setState(state => {
            return {
                edit: {
                    ...state.edit,
                    newProperty: {
                        selected: false
                    },
                    addPropertyButtonIsActive: false
                }                
            }

        });
    }

    acceptAddingProperty = () => {
        this.setState(state => {
            return {
                edit: {
                    ...state.edit,
                    properties: [...state.edit.properties, state.edit.newProperty],
                    newProperty: null,
                    addPropertyButtonIsActive: true
                }                
            }
        });
    }

    cancelAddingProperty = () => {
        this.setState(state => {
            return {
                edit: {
                    ...state.edit,
                    newProperty: null,
                    addPropertyButtonIsActive: true
                }                
            }
        });
    }

    preparePropertiesForDropDown = () => {
        var selectedProperties = [...this.state.edit.properties];
        var selectableProperties = this.props.selectableProperties.filter(item => {
            return selectedProperties.findIndex(selectedItem => item.alias === selectedItem.alias) < 0;
        });

        return selectableProperties.map(p => {
            return {
                value: p.alias,
                label: p.label,
            };
        });
    }

    propertyChanged = (value) => {
        this.setState(state => {
            var newProperty;
            if(value) {
                newProperty = {
                    selected: true,
                    alias: value.value, 
                    label: value.label 
                };
            } else {
                newProperty = {
                    selected: false
                };
            }
            return {
                edit: {
                    ...state.edit,
                    newProperty: newProperty
                }
            }
        });
    }

    removeProperty = (property) => {
        this.setState(state => {
            return {
                edit: {
                    ...state.edit,
                    properties: state.edit.properties.filter(p => p !== property)
                }
            }
        });
    }

    onSave = () => {
        this.props.onSave({
            id: this.state.edit.id,
            description: this.state.edit.description,
            properties: this.state.edit.properties.map((item) => {
                return item.alias;
            })
        });
    }

    removeProductType = () => {
        this.props.removeProductType(this.props.type.id);
    }

    render() {
        const { type } = this.props;

        var justShow = (
            <div className="datasheet-list-item">                             
                Описание:
                <span className="datasheet-list-item__field">
                    {type.description}
                </span>
                <div className="datasheet-list-item__right-section">
                    <button className="button datasheet-list-item__button" onClick={this.startLineEditing}>Изменить</button>
                    <button className="button datasheet-list-item__button" onClick={this.removeProductType}>Удалить</button>
                </div>
            </div>
        );

        if (this.state.edit) {
            justShow = (
                <div className="datasheet-list-item">
                    Описание: 
                    <input className="edit-form__editable datasheet-list-item__input" type="text" value={this.state.edit.description} onChange={(e) => this.onChange(e, "description")}/>
                    <div className="datasheet-list-item__right-section">
                        <button className="button datasheet-list-item__button" onClick={this.onSave}>Сохранить</button>
                        <button className="button datasheet-list-item__button" onClick={() => this.cancelLineEditing()}>Отмена</button>
                    </div>
                    <div className="product-types__property-section">
                        {this.state.edit.properties.map(p => {
                            return (
                                <PropertyLine {...p} removeProperty={() => this.removeProperty(p)} key={p.alias}/>
                            );
                        })}
                        {this.state.edit.newProperty && (
                            <AddPropertyLine propertyChanged={this.propertyChanged} 
                            cancelAddingProperty={this.cancelAddingProperty} 
                            acceptAddingProperty={this.acceptAddingProperty} 
                            addButtoyIsActive={this.state.edit.newProperty.selected} 
                            properties={this.preparePropertiesForDropDown()}/>
                        )}
                        <button className="button product-types__add-btn" disabled={!this.state.edit.addPropertyButtonIsActive} onClick={this.addProperty}>Добавть свойство</button>
                    </div>
                </div>
            );
        }

        return justShow;
    }
}

function PropertyLine(props) {


    return ( 
        <div className="datasheet-list-item">
            Свойство:
            <span className="datasheet-list-item__field">
                {props.label}
            </span>
            <div className="datasheet-list-item__right-section">
                <button className="button datasheet-list-item__button" onClick={props.removeProperty}>Удалить</button>
            </div>
        </div>
    );


};

function  AddPropertyLine(props) {


        return ( 
            <div className="datasheet-list-item">
                Свойство:
                <div className="datasheet-list-item__dropdown-section">
                    <HierarchicalDropdown items={props.properties} className="product-type-item__dropdown" onChange={props.propertyChanged}/>
                </div>            
                <div className="datasheet-list-item__right-section">
                    <button className="button datasheet-list-item__button" disabled={!props.addButtoyIsActive} onClick={props.acceptAddingProperty}>Добавить</button>
                    <button className="button datasheet-list-item__button" onClick={props.cancelAddingProperty}>Отмена</button>
                </div>
            </div>
        );

}

ProductTypeItem.propTypes = {
    type: PropTypes.shape({
        id: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        properties: PropTypes.arrayOf(PropTypes.shape({
            alias: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired
          })).isRequired,
    }),   
    selectableProperties: PropTypes.arrayOf(PropTypes.shape({
        alias: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
      })).isRequired
};

  
export default ProductTypeItem;