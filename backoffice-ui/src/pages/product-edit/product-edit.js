import React, { Component, PureComponent } from 'react';
import './product-edit.css';
import ImageZoom from 'react-medium-image-zoom';
import MainTitle from '../../components/main-title';
import HierarchicalDropdown from '../../components/hierarchical-dropdown';
import NewImage from '../../components/new-image';
import DataAccess from '../../services/dataAccess';
import { ProductLabel, ProductPrice, ProductStockBalance, ProductIsHidden } from '../../components/product-form-items';
import { stat } from 'fs';

class ProductEditPage extends Component {

  constructor(props)
  {
      super(props);

      this.state = {
        product: this.createProductTemplate(),
        categories: null,
        productTypes: [],    
        newImageList: [], //добавленные новые изображения
        wasAttemptToSend: false,
        isLoading: true
      };
  }

  componentDidMount = () => {
    var receiveProduct_receiveAllProductTypes_Promise =  Promise.all([this.receiveProduct(), this.receiveAllProductTypes()]).then(this.initProperties);
    Promise.all([receiveProduct_receiveAllProductTypes_Promise, this.receiveCategories()]).then(() => {
      this.setState((state) => {
        return {
          isLoading: false
        }
      });
    });
  }

  receiveAllProductTypes = () => {
    return DataAccess.getAllProductTypes()
        .then(data => {
            this.setState({ productTypes: data });
        });
  }

  receiveProduct = () => {
    const id = this.props.match.params.id;
    return DataAccess.getProductToEdit(this.props.match.params.id)
      .then(this.mapProductFromAPI);
  }

  receiveCategories = () => {
    return DataAccess.getCategoriesForSelect()
      .then(data => {
          data.unshift({label: 'Без категории', value: '_without'});
          this.setState({ categories: data });
      });
  }

  createProductTemplate = () => {
    return {
      categoryId: null,
      productTypeId: null,
      label: { value: "", valid: true },
      price: { value: 0, valid: true },
      stockBalance: { value: 0, valid: true },
      isHidden: false,
      properties: [],
      initialValues: [],
      images: [] // загруженные ранее изображения
    };
  }

  mapProductFromAPI = (data) => {
    var product = { ...this.state.product };
    product.categoryId = data.categoryId;
    product.productTypeId = data.productTypeId;
    product.label = { ...this.state.product.label, value: data.label };
    product.price = { ...this.state.product.price, value: data.price };
    product.stockBalance = { ...this.state.product.stockBalance, value: data.stockBalance };

    product.initialValues = data.values ? data.values : product.initialValues;

    product.images = data.images ? data.images.map((image) => {
      return {
        path: image.path,
        isPreview: image.isPreview,
        hasToBeRemoved: false
      };
    }) : product.images;
    
    this.setState({ product });
  }

  mapValuesToProperies = (valuesList, properiesList) => {
    var result = [];

    properiesList.forEach((property) => {
      let valueItem = valuesList.find((value) => value.alias === property.alias);

      result.push({
        alias: property.alias,
        label: property.label,
        value: valueItem ?  valueItem.value : "",
      });
    });

    return result;
  }

  categoryIdOnChange = (data) => {
    this.setState({ product: {...this.state.product, categoryId: data.value}});
  }

  labelOnChange = (data) => {
    this.setState({ product: {...this.state.product, label: data}});
  }

  priceOnChange = (data) => {
    this.setState({ product: {...this.state.product, price: data}});
  }

  stockBalanceOnChange = (data) => {
    this.setState({ product: {...this.state.product, stockBalance: data}});
  }

  isHiddenOnChange = (data) => {
    this.setState({ product: {...this.state.product, isHidden: data}});
  }

  productTypeOnChange = (option) => {
    var selectedProductType = null;
    
    if(option) {
      selectedProductType = this.findProductTypeByID(option.value);    
    }    
    
    this.setState((state) => {
      return {
        product: {
          ...state.product,
          productTypeId: selectedProductType && selectedProductType.id,
          properties: selectedProductType ? this.mapValuesToProperies(state.product.initialValues , selectedProductType.properties) : []
        } 
      }
    });
  }

  initProperties = () => {
    if(this.state.product.productTypeId) {
      let currentProductType = this.findProductTypeByID(this.state.product.productTypeId);

      this.setState((state) => {

        return  {
          product: {
            ...state.product,
            properties: this.mapValuesToProperies(state.product.initialValues , currentProductType.properties)
          } 
        }
      });
    }            
  }

  findProductTypeByID = (id) => {
    return this.state.productTypes.find((item) => {
      return item.id === id;
    });
  }

  propertyValueOnChange = (propItem, value) => {
    this.setState((state) => {
      return {
        product: {
          ...state.product,
          properties: state.product.properties.map(item => item === propItem ? {...item, value } : item)
        }
      }
    });
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.setState({wasAttemptToSend: true});

    const formData = new FormData();

    formData.append("SelectedCategoryId", this.state.product.categoryId);
    formData.append("Label", this.state.product.label.value);
    formData.append("Price", this.state.product.price.value);
    formData.append("StockBalance", this.state.product.stockBalance.value);
    formData.append("IsHidden", this.state.product.isHidden);

    this.state.newImageList.forEach(image => {
      formData.append('NewImages', image.file);
    });

    this.state.product.images.forEach((imageFile, index) => {
      formData.append('PersistedImages[' + index + '].path', imageFile.path);
      formData.append('PersistedImages[' + index + '].hasToBeRemoved', imageFile.hasToBeRemoved);
      formData.append('PersistedImages[' + index + '].isPreview', imageFile.isPreview);
    });

    DataAccess.putProductToEdit(this.props.match.params.id, formData)
    .then((response) => {
      this.props.history.push("/backoffice/products");
    }).catch((error) => {
      console.log(error.message);
    });
  }

  newImagesListChange = (newImageList) => {
    this.setState({ newImageList });
  }

  uploadedImageOnClickHandler = (key, image) => {
    this.setState((state) => {
      const images = state.product.images.map((imageItem) => {
        if(imageItem === image) {
          let result = { ...imageItem };
          result[key] = !result[key];
          return result;
        }            
        return imageItem;
      });
      return { product: { ...state.product, images } };
    });
  }

  removeProductOnClickHandler = (event) => {
    event.preventDefault();
    DataAccess.removeProduct(this.props.match.params.id)
      .then(() => {
        window.location.href = "/backoffice/products";
      });
  }

  getProductTypeSelectItems = () => {
    var options = this.state.productTypes.map(item => {
      return {
        label: item.description,
        value: item.id
      };
    });

    return options;
  }

  render = () => {
    const { product, categories, wasAttemptToSend }= this.state;
    return (
      <main className="layout__main">
        <MainTitle>Управление магазином</MainTitle>
        { !this.state.isLoading && <div className="flex-grid product-edit">
          <div className="flex-grid__item product-edit__form-panel">
            <form onSubmit={this.onSubmit}>
              <ul className="edit-form">
                <li className="edit-form__row">
                	<label className="edit-form__label" htmlFor="SelectedCategoryId">Принадлежность к категории</label>
                	<div className="edit-form__edit-section">
                	  	<HierarchicalDropdown items={categories} initiallySelectedValue={product.categoryId} onChange={this.categoryIdOnChange}/>
                	</div>
                </li>
                <li className="edit-form__row">
                  <ProductLabel value={product.label.value} onChange={this.labelOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                	<ProductPrice value={product.price.value} onChange={this.priceOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                	<ProductStockBalance value={product.stockBalance.value} onChange={this.stockBalanceOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                	<ProductIsHidden value={product.isHidden} onChange={this.isHiddenOnChange}/>
                </li>
                <li className="edit-form__row">
                  <label className="edit-form__label" htmlFor="SelectedCategoryId">Дополнительные поля</label>
                  <div className="edit-form__edit-section">          
                    <HierarchicalDropdown items={this.getProductTypeSelectItems()} onChange={this.productTypeOnChange} initiallySelectedValue={product.productTypeId}/>
                  </div>
                </li>
                {console.log(this.state.product)}
                {this.state.product.properties.map(item => {
                  return(
                    <li className="edit-form__row" key={item.alias}>
                    	<label className="edit-form__label" htmlFor={item.alias}>{item.label}</label>
                      <div className="edit-form__edit-section">
                        <input className="edit-form__editable edit-form__editable_stretched" type="text" id={item.alias} name={item.alias} value={item.value} onChange={(e) => this.propertyValueOnChange(item, e.target.value)}/>
                      </div>		    		          
                    </li>
                  );
                })}
                <li className="edit-form__row">
                	<NewImage newImagesListChange={this.newImagesListChange}/>
                </li>
                <li className="edit-form__row">
                    <div className="preview-imgs">
                      {/* добавленные новые изображения */}
                      {this.state.newImageList.map((image) => { 
                          return (
                            <ImageZoom
                            key={image.key}
                            image={{
                              src: image.objURL,
                              alt: image.name,
                              className: 'preview-imgs__item',
                            }}
                            zoomImage={{
                              src: image.objURL,
                              alt: image.name,
                            }}/>                       
                          );
                      })}
                    </div>
                </li>
                <li className="edit-form__row">
                  <button id="remove-product" className="button button_rd" onClick={this.removeProductOnClickHandler}>Удалить</button>
                  <div className="edit-form__edit-section edit-form__edit-section_al-r">
                    <a className="button" href="/backoffice/products">Отмена</a>
                    <button type="submit" className="button button_gr product-edit__submit-btn">Сохранить</button>
                  </div>
                </li>
              </ul>
            </form>
          </div>
          {/* загруженные ранее изображения */}
          {product.images.map((image) => {
            return (
              <div className="flex-grid__item product-edit__image-tile">
                <ImageZoom
                  image={{
                    src: image.path,
                    alt: product.label.value,
                    className: "product-edit__image"
                  }}
                  zoomImage={{
                    src: image.path,
                    alt: product.label.value,
                  }}/>
                <i className={`fa fa fa-remove fa-lg product-edit__image-remove ${image.hasToBeRemoved ? 'product-edit__image-remove_red' : null}`} title="Пометить на удаление" onClick={() => this.uploadedImageOnClickHandler("hasToBeRemoved", image)}></i>
                <i className={`fa fa-check fa-lg product-edit__image-add-preview ${image.isPreview ? 'product-edit__image-add-preview_green' : null}`} title="Пометить как предпросмотр" onClick={() => this.uploadedImageOnClickHandler("isPreview", image)}></i>
              </div>
            );
          })}
        </div>
      }            
      </main>
    )
  }
}

export default ProductEditPage;
