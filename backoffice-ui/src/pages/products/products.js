import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './products.css';
import DataAccess from '../../services/dataAccess';
import TopPanel from '../../components/top-panel';
import MainTitle from '../../components/main-title';
import ProductTile  from '../../components/product-tile';
import HierarchicalDropdown from '../../components/hierarchical-dropdown';

class ProductsPage extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          products: [],
          categories: []
        };
    }
    componentDidMount = () => {
        this.retviveProducts();
        this.retviveCategories();
    }

    retviveProducts = (params) => {
        // http://localhost:11574/backoffice/api/products?categoryId=men-clothing
        var query = '';
        Object.keys(params || {}).forEach((key, index) => {
            if(index === 0) {
                query += '?';
            } else if (index > 0) {
                query += '&';
            }
            query += key + '=' + params[key];
        });
        fetch('http://localhost:11574/backoffice/api/products' + query)
        .then(response => response.json())
        .then(data => {
            this.setState({ products: data });
        });
    }

    retviveCategories = () => {

        DataAccess.getCategoriesForSelect()
        .then(data => {
            data.unshift({label: 'Без категории', value: '_without'});
            this.setState({ categories: data });
        });
    }

    categoryChangeHandler = (selectedItem) => {
        if(selectedItem) {
            this.retviveProducts({
                categoryId: selectedItem.value
            });
        } else {
            this.retviveProducts();
        }
    }

    render() {
        const { products } = this.state;

        return (
            <main className="layout__main">
                <MainTitle>Управление магазином</MainTitle>
                <TopPanel items={[
                    <Link to="/backoffice/products/add" className="top-panel__button">Добавить товар</Link>,
                    <label htmlFor="category-sel-product-filter">
                        Фильтр по категориям:
                        <HierarchicalDropdown items={this.state.categories} onChange={this.categoryChangeHandler}/>
                    </label>
                ]} />
                <div className="flex-grid products-list">
                    {products.map(product => <ProductTile key={product.id} product={product}/>)}
                </div>
            </main>
        );
    }
}

  
export default ProductsPage;