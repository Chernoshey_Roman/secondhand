import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './properties.css';
import DataAccess from '../../services/dataAccess';
import TopPanel from '../../components/top-panel';
import MainTitle from '../../components/main-title';

class PropertiesPage extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            properties: []
        };
    }    

    createProperty = (p) => {        
        return {alias: p.alias, label: p.label, editing: false};
    }

    createEditingProperty = (p) => {
        return {alias: p.alias, label: p.label, editableAlias: p.alias, editableLabel: p.label,  editing: true, isNewItem: false, validWorn: false};
    }

    createNewEditingProperty = (p) => {
        return {alias: p.alias, label: p.label, editableAlias: p.alias, editableLabel: p.label,  editing: true, isNewItem: true,  validWorn: false};
    }

    stopSuccessfullyEditingProperty = (p) => {
        return {alias: p.editableAlias, label: p.editableLabel, editing: false};
    }

    getEditableData = (p) => {
        return {alias: p.editableAlias, label: p.editableLabel};
    }

    componentDidMount = () => {
        this.retviveProperties();
    }
    
    retviveProperties = () => {
        DataAccess
            .getAllProperties()
            .then(data => {
                var properties = data || [];
                this.setState({ 
                        properties: properties.map(p => { return this.createProperty(p)})
                });
            })
    }

    onChange = (event, object, field) => {
        var target = event.currentTarget;
        this.setState(state => {
            return {
                properties: state.properties.map(p => p === object ? {...p, [field]: target.value} : p )
            };
        });
    }

    makeEditable = (object) => {
        this.setState(state => {
            return {
                properties: state.properties.map(p => p === object ? this.createEditingProperty(p) : p )
            };
        });
    }

    cancelEdit = (object) => {
        this.setState(state => {
            return {
                properties: state.properties.map(p => p === object ? this.createProperty(p) : p )
            };
        });
    }

    cancelNewEdit = (object) => {        
        this.setState(state => {
            var index = state.properties.indexOf(object);
            state.properties.splice(index, 1);
            return {
                properties: [...state.properties]
            };
        });
    }

    isValid = (object) => {
        var isValid = true;
        isValid = !!object.editableAlias && !!object.editableLabel && /^[\w-]{1,30}$/.test(object.editableAlias);        

        this.setState(state => {
            return {
                properties: state.properties.map(p => p === object ? {...p, validWorn: !isValid} : p )
            };
        });        

        return isValid;
    }

    save = (object) => {
        if(this.isValid(object)) {
            DataAccess
            .saveProperty(object.alias, this.getEditableData(object))
            .then(() => {
                this.setState({
                    properties: this.state.properties.map(p => p === object ? this.stopSuccessfullyEditingProperty(p) : p)
                });
            });
        }
    }

    saveNew = (object) => {
        if(this.isValid(object)) {
            DataAccess
            .addNewProperty(this.getEditableData(object))
            .then(() => {
                this.setState({
                    properties: this.state.properties.map(p => p === object ? this.stopSuccessfullyEditingProperty(p) : p)
                });
            });
        }
    }

    remove = (object) => {
        DataAccess
        .removeProperty(object.alias)
        .then(() => {
            this.setState(state => {
                var index = state.properties.indexOf(object);
                state.properties.splice(index, 1);
                return {
                    properties: [...state.properties]
                };
            });
        });
    }

    createNewProperty = () => {
        this.setState(state => {
            return {
                properties: [...state.properties, this.createNewEditingProperty({alias: "", label: ""})]
            };
        });
    }

    render() {
        const { properties } = this.state;
        return (
            <main className="layout__main">
                <MainTitle>Управление свойствами товаров</MainTitle>
                <TopPanel items={[
                    <button className="button top-panel__button" onClick={this.createNewProperty}>Добавить новое свойство</button>
                ]} />
                <div className="flex-grid">
                    <div className="flex-grid__item">
                        {properties.map((property) => {
                            return !property.editing ? (
                            <div className="datasheet-list-item" key={property.alias}>
                                Уникальный идентификатор:
                                <span className="datasheet-list-item__field">
                                     {property.alias}
                                </span>
                                Описание:
                                <span className="datasheet-list-item__field">
                                    {property.label}
                                </span>
                                <div className="datasheet-list-item__button-section">
                                    <button className="button datasheet-list-item__button" onClick={() => this.makeEditable(property)}>Изменить</button>
                                    <button className="button datasheet-list-item__button" onClick={() => this.remove(property)} >Удалить</button>
                                </div>
                            </div>
                            ) : (
                                <div className={"datasheet-list-item" + (property.validWorn ? " datasheet-list-item_invalid" : "")} key={property.alias}>
                                Уникальный идентификатор:

                                     <input className="edit-form__editable datasheet-list-item__input" type="text" value={property.editableAlias} onChange={(e) => this.onChange(e, property, "editableAlias")}/>

                                Описание:

                                    <input className="edit-form__editable datasheet-list-item__input" type="text" value={property.editableLabel} onChange={(e) => this.onChange(e, property, "editableLabel")}/>

                                <div className="datasheet-list-item__button-section">
                                    <button className="button datasheet-list-item__button" onClick={() => property.isNewItem ? this.saveNew(property) : this.save(property)} >Сохранить</button>
                                    <button className="button datasheet-list-item__button" onClick={() => property.isNewItem ? this.cancelNewEdit(property) : this.cancelEdit(property)}>Отмена</button>
                                </div>
                            </div>
                            );
                        })}
                    </div>
                </div>
            </main>
        );
    }
}

  
export default PropertiesPage;