import React, { Component } from 'react';
import './product-add.css';
import ImageZoom from 'react-medium-image-zoom';
import MainTitle from '../../components/main-title';
import HierarchicalDropdown from '../../components/hierarchical-dropdown';
import NewImage from '../../components/new-image';
import DataAccess from '../../services/dataAccess';
import { ProductLabel, ProductPrice, ProductStockBalance, ProductIsHidden } from '../../components/product-form-items';

class ProductAddPage extends Component {

  constructor(props)
  {
      super(props);

      this.state = {
        product: this.createFakeProduct(),
        categories: null,
        newImageList: [], //добавленные новые изображения
        wasAttemptToSend: false
      };
  }

  componentDidMount = () => {
    this.receiveCategories();
  }

  receiveCategories = () => {
    DataAccess.getCategoriesForSelect()
        .then(data => {
            data.unshift({label: 'Без категории', value: '_without'});
            this.setState({ categories: data });
        });
  }

  createFakeProduct = () => {
    return {
      categoryId: { value: null, valid: true },
      label: { value: "", valid: true },
      price: { value: 0, valid: true },
      stockBalance: { value: 0, valid: true },
      isHidden: { value: false, valid: true }
    };
  }

  categoryIdOnChange = (data) => {
    this.setState({ product: {...this.state.product, categoryId: {
      value: data.value,
      valid: true
    }}});
  }

  labelOnChange = (data) => {
    this.setState({ product: {...this.state.product, label: data}});
  }

  priceOnChange = (data) => {
    this.setState({ product: {...this.state.product, price: data}});
  }

  stockBalanceOnChange = (data) => {
    this.setState({ product: {...this.state.product, stockBalance: data}});
  }

  isHiddenOnChange = (data) => {
    this.setState({ product: {...this.state.product, isHidden: data}});
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.setState({wasAttemptToSend: true});

    const formData = new FormData();

    formData.append("SelectedCategoryId", this.state.product.categoryId.value);
    formData.append("Label", this.state.product.label.value);
    formData.append("Price", this.state.product.price.value);
    formData.append("StockBalance", this.state.product.stockBalance.value);
    formData.append("IsHidden", this.state.product.isHidden.value);

    this.state.newImageList.forEach(image => {
      formData.append('NewImages', image.file);
    });

    DataAccess.postProductToAdd(formData)
    .then((response) => {
      window.location.href = "/backoffice/products";
    }).catch((error) => {
      console.log(error.message);
    });
  }


  newImagesListChange = (newImageList) => {
    this.setState({ newImageList });
  }

  render = () => {
    let { product, categories, wasAttemptToSend }= this.state;
    return (
      <main className="layout__main">
        <MainTitle>Управление магазином</MainTitle>
        <div className="flex-grid product-edit">
          <div className="flex-grid__item product-edit__form-panel">
            <form onSubmit={this.onSubmit}>
              <ul className="edit-form">
                <li className="edit-form__row">
                  <label className="edit-form__label" htmlFor="SelectedCategoryId">Принадлежность к категории</label>
                  <div className="edit-form__edit-section">
                    <HierarchicalDropdown items={categories} initiallySelectedValue={product.categoryId.value} onChange={this.categoryIdOnChange}/>
                  </div>
                </li>
                <li className="edit-form__row">
                  <ProductLabel value={product.label.value} onChange={this.labelOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                  <ProductPrice value={product.price.value} onChange={this.priceOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                  <ProductStockBalance value={product.stockBalance.value} onChange={this.stockBalanceOnChange} wasAttemptToSend={wasAttemptToSend}/>
                </li>
                <li className="edit-form__row">
                  <ProductIsHidden value={product.isHidden.value} onChange={this.isHiddenOnChange}/>
                </li>
                <li className="edit-form__row">
                  <NewImage newImagesListChange={this.newImagesListChange}/>
                </li>
                <li className="edit-form__row">
                    <div className="preview-imgs">
                      {/* добавленные новые изображения */}
                      {this.state.newImageList.map((image) => { 
                          return (
                            <ImageZoom
                            key={image.key}
                            image={{
                              src: image.objURL,
                              alt: image.name,
                              className: 'preview-imgs__item',
                            }}
                            zoomImage={{
                              src: image.objURL,
                              alt: image.name,
                            }}/>                       
                          );
                      })}
                    </div>
                </li>
                <li className="edit-form__row">
                  <div className="edit-form__edit-section edit-form__edit-section_al-r">
                    <a className="button" href="/backoffice/products">Отмена</a>
                    <button type="submit" className="button button_gr product-edit__submit-btn">Сохранить</button>
                  </div>
                </li>
              </ul>
            </form>
          </div>                
        </div>               
      </main>
    )
  }
}

export default ProductAddPage; 
