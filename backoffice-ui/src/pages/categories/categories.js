import React, { Component } from 'react';
import { Route, Link, NavLink } from 'react-router-dom';
import { Treebeard } from 'react-treebeard';
import DataAccess from '../../services/dataAccess';
import './categories.css';
import CategoryEditForm from "../../components/category-forms/category-edit-form";
import CategoryAddChildForm from "../../components/category-forms/category-addchild-form";
import CategoryAddRootForm from "../../components/category-forms/category-addroot-form";

class CategoriesPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tree: []
        };
    }

    style = {
        tree: {
            base: {
                backgroundColor: 'transparent',
                color: '#999'
            }
        }        
    }

    componentDidMount = () => {
        this.getCategoryTree();
    }

    componentDidUpdate = () => {        
        console.log(this);
    }

    getCategoryTree = () => {
        DataAccess
            .getCategoryTree()
            .then(this.convertCategoryTree)
            .then((data) => {
                this.setState({ tree: data});
            });
    }

    convertCategoryTree = (data) => {
        return (function recursive(categories) {
            return categories.map(item => {
                var child = (!!item.child && item.child.length && recursive(item.child)) || null;
                return {
                    id: item.id,
                    name: item.name,
                    children: child,
                    toggled: !!child
                };
            });
        })(data);
    }

    onToggle = (node, toggled) => {
        var { match, history } = this.props;
        if (this.state.cursor) { this.state.cursor.active = false; }
        node.active = true;
        this.setState({ cursor: node }, () => {
            history.push(`${match.url}/${node.id}/edit`);          
        });               
    }


    render = () => {
        const match = this.props.match;
        const addRootRout = `${match.path}/add`;
        const editRoute = `${match.path}/:id/edit`;
        const addRoute = `${match.path}/:id/add`;

        return (
            <main className="layout__main">
                <h1 className="layout__main-title">Управление магазином</h1>
                <div className="top-panel">
                    <div className="top-panel__item">                        
                        <Link to={addRootRout} className="top-panel__button">Добаветь коренную категорию</Link>
                    </div>
                </div>
                <div className="flex-grid categories-box">
                    <div className="flex-grid__item categories-box__left">
                        <h2 className="flex-grid__item-title">Дерево категорий</h2>
                        <div className="categories-box__container">
                            <Treebeard
                                style={this.style}
                                data={this.state.tree}
                                onToggle={this.onToggle}
                            />
                        </div>
                    </div>
                    <div className="flex-grid__item categories-box__right">

                        <Route exact path={addRootRout} render={(props) => {
                            return (
                                <div className="flex-grid__item-tab-container">
                                    <Link to={addRootRout} className="flex-grid__item-tab flex-grid__item-tab_active">Добаветь коренную категорию</Link>
                                </div>
                            );
                        }}/>

                        <Route exact path={[editRoute, addRoute]} render={(props) => {
                            return (
                                <div className="flex-grid__item-tab-container">
                                    <NavLink className="flex-grid__item-tab" activeClassName="flex-grid__item-tab_active" to={`${match.url}/${props.match.params.id}/edit`}>Изменить</NavLink>
                                    <NavLink className="flex-grid__item-tab" activeClassName="flex-grid__item-tab_active" to={`${match.url}/${props.match.params.id}/add`}>Добавить категрию</NavLink>
                                </div>
                            );
                        }}/>
                        <Route exact path={addRootRout} component={CategoryAddRootForm} />
                        <Route exact path={editRoute} component={CategoryEditForm} />
                        <Route exact path={addRoute} component={CategoryAddChildForm} />
                        <Route exact path={match.path} render={() => <h3>Please select a category.</h3>} /> 
                    </div>                 
                </div>
            </main>
        )
    }
};

export default CategoriesPage;